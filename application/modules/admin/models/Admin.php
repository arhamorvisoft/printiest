<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Model {

 /**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

 public function __construct()
 {
  parent::__construct();
 }

 public function verifyLogin($post){
 return $this->db->get_where('admin',array('password' => md5($post['pass']), 'email' => $post['email']))->row_array();
 }

 public function orders_list(){
 return $this->db->select('*')
      ->from('orders')
      ->join('order_items', 'orders.id = order_items.order_id')
      ->join('order_payment', 'orders.id = order_payment.order_id')
      ->join('order_shipment', 'order_shipment.id = orders.order_shipment_id')
      ->Order_by('orders.id' , 'desc')
      ->get()
      ->result_array();
  }

 }


