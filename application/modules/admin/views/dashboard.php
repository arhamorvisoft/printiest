<?php

/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>

<?php $this->load->view('common/header', $header); ?>

<div class="main-wrap">

  <aside class="user-menu">

    <div class="tabs-offcanvas">
      <ul class="nav nav-tabs nav-justified">
        <li class="active"><a href="#userbar-one" data-toggle="tab">Main</a></li>
      </ul>
      <div class="tab-content">


        <div class="tab-pane active" id="userbar-one">
          <div class="main-info">
            <div class="user-img"><img src="" alt="" /></div>
            <h1><?php //echo $data['name']; ?> <small>Administrator</small></h1>
          </div>
          <div class="list-group"> <a href="" class="list-group-item"><i class="fa fa-user"></i>Profile</a> <a href="" class="list-group-item"><i class="fa fa-cog"></i>Settings</a>

            <div class="empthy"></div>
            <a href="<?php echo site_url('admin/index/logout'); ?>" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a> </div>
        </div>

      </div>
    </div>
  </aside>

  <?php $this->load->view('common/left'); ?>

  <div class="content-wrapper">
    <?php if($this->session->flashdata('submission_success') == 'yes'): ?>
    <div class="alert alert-success alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
      <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
    </div>
    <?php elseif($this->session->flashdata('submission_success') == 'no'): ?>
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
      <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
    </div>
    <?php endif; ?>
    <?php $this->load->view($body['template'], $body['data']); ?>
  </div>
  <?php $this->load->view('common/footer', $footer); ?>
