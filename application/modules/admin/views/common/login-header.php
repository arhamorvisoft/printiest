<?php

/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>


<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<link href="<?php echo base_url('assets/css/styles.css'); ?>" rel="stylesheet" type="text/css">

<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/favicon.ico'); ?>" />
<script type="text/javascript" src="<?php echo base_url('js/vendors/horisontal/modernizr.custom.js'); ?>"></script>
</head>

<body>
<div class="colorful-page-wrapper">
  <div class="center-block">
