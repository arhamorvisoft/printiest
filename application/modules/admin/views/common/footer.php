<?php

/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>

</div>
</div>
<div class="scroll-top-wrapper hidden-xs"> <i class="fa fa-angle-up"></i> </div>
<div class="modal" id="delete-widget">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-lock"></i> </div>
      <div class="modal-body text-center">
        <p>Are you sure to delete this widget?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancel</button>
        <button type="button" class="btn btn-primary" id="trigger-deletewidget">Delete</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="signout">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-lock"></i> </div>
      <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="yesigo">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="lockscreen">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-lock"></i> </div>
      <div class="modal-body text-center">Are You Sure Want To Lock Screen?</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="yesilock">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/jquery/jquery-ui.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/gmap/jquery.gmap.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/easing/jquery.easing.1.3.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/easypie/jquery.easypiechart.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/fullscreen/screenfull.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/nanoscroller/jquery.nanoscroller.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/sparkline/jquery.sparkline.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/powerwidgets/powerwidgets.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/raphael/raphael-min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/morris/morris.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/flotchart/jquery.flot.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/flotchart/jquery.flot.resize.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/flotchart/jquery.flot.axislabels.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/flotchart/jquery.flot-tooltip.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/chartjs/chart.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/fullcalendar/fullcalendar.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/fullcalendar/gcal.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/bootstrap/bootstrap.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/vector-map/jquery.vmap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/vector-map/jquery.vmap.sampledata.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/vector-map/jquery.vmap.world.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/todos/todos.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/skycons/skycons.js'); ?>"></script>
<script>
  var icons = new Skycons({"color": "#fff"}),
      list  = [
        "clear-day", "clear-night", "partly-cloudy-day",
        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
        "fog"
      ],
      i;
  for(i = list.length; i--; )
    icons.set(list[i], list[i]);

  icons.play();
</script>

<script type="text/javascript" src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>
<?php
$data = $this->session->flashdata('post_data');
if(strlen($data) > 0):
$data = json_decode($data, true);
?>
<script>
  <?php foreach($data as $key=>$val): ?>
  $('input[name=<?php echo $key; ?>]').val('<?php echo $val ?>');
  <?php endforeach; ?>
</script>
<?php endif;?>



</body>
</html>

