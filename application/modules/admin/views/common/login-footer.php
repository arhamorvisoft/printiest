<?php

/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>

</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/jquery/jquery-ui.min.js'); ?>'"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/forms/jquery.form.min.js'); ?>'"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/forms/jquery.validate.min.js'); ?>'"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/forms/jquery.maskedinput.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/jquery-steps/jquery.steps.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/nanoscroller/jquery.nanoscroller.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendors/sparkline/jquery.sparkline.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>


</body>
</html>
