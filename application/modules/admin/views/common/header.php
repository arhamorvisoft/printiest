<?php

/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>

<!DOCTYPE html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <meta name="keywords" content="<?php echo @$keywords; ?>">
  <meta name="author" content="eComHut Solutions">
  <meta name="description" content="<?php echo @$description; ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title; ?></title>
  <link href="<?php echo base_url('assets/css/styles.css'); ?>" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/favicon.ico'); ?>" />
  <script type="text/javascript" src="<?php echo base_url('assets/js/vendors/modernizr/modernizr.custom.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/vendors/jquery/jquery.min.js'); ?>"></script>
</head>

<body>
  <div class="smooth-overflow">
    <nav class="main-header clearfix" role="navigation"> <a class="navbar-brand" href="index.html<?php echo site_url(); ?>"><span class="text-blue">Printiest.pk</span></a>
      <div class="navbar-content">
        <a href="#" class="btn btn-default left-toggler"><i class="fa fa-bars"></i></a>
        <a href="#" class="btn btn-user right-toggler pull-right"><i class="entypo-vcard"></i> <span class="logged-as hidden-xs">Logged as</span><span class="logged-as-name hidden-xs">Arham</span></a>
        <div class="btn-group pull-right">

 <button type="button" class="btn btn-default" title="Logout"  onclick="window.location='<?php echo site_url('admin/index/logout'); ?>'"> <i class="entypo-logout"></i></button>
        </div>

<div class="btn-group"></div>
      </div>
    </nav>
