<?php
/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>

<?php if(isset($Users)&&(is_array($Users) && count($Users))):?>
<div id="powerwidgets" class="row">
  <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable">
    <div data-widget-editbutton="false" id="priceblocktable" class="powerwidget cold-grey powerwidget-sortable" style="" role="widget">
      <header role="heading">
        <h2>Users<small>List</small></h2>
        <div class="powerwidget-ctrls" role="menu"> <a class="button-icon powerwidget-delete-btn" href="#"><i class="fa fa-times-circle"></i></a>  <a class="button-icon powerwidget-fullscreen-btn" href="#"><i class="fa fa-arrows-alt "></i></a> <a class="button-icon powerwidget-toggle-btn" href="#"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
      <div class="inner-spacer" role="content">
        <a href="<?php echo site_url('admin/index/add_user') ?>" class="btn btn-success pull-right">Add New</a>
        <div class="pricing-table" style="clear:both; padding-top:5px">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>User Name</th>

                <th>Email</th>
                <th>Status</th>
                <th>gender</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($Users as $key => $value): ?>
              <tr>
                <td class="features"><?php echo $value['name']; ?></td>

                <td class="features"><?php echo $value['email']; ?></td>
                <td class="<?php $status = ($value['status'] == '1') ? true:false; ?><?php echo $status?'yes':'no'; ?>"><?php echo $status? 'Active': 'In-Active'; ?></td>
                <td class="<?php $gender = ($value['gender'] == '1') ? 1:0; ?><?php echo $gender?'1':'0'; ?>"><?php echo $gender? 'male': 'female'; ?></td>
                <td class="no"> <a href="<?php echo site_url('admin/index/update_status/'.$value['id'].'/'.($value['status'] == '1'? 0 : 1)) ?>" title="Update"><i class="fa fa-exchange"></i></a> &nbsp; <a onclick="return confirm('Are you sure?')" href="<?php echo site_url('admin/index/delete_user/'.$value['id']) ?>" title="DELETE"><i class="fa fa-trash-o"></i></a></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
<h1>Sorry, No User Avilable in the system. <a href="<?php echo site_url('admin/index/add_user') ?>">Create</a> now.</h1>
<?php endif; ?>
