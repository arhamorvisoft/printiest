<?php
/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>

<div class="login-block">
  <form action="<?php echo site_url('admin/index/login_check'); ?>" id="login-form" class="orb-form" method="post">
    <header>
      <div class="image-block"><img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="User" /></div>
      Login to Printiest.pk..!</header>
    <fieldset>
      <section>
        <div class="row">
          <label class="label col col-4">Email</label>
          <div class="col col-8">
            <label class="input"> <i class="icon-append fa fa-user"></i>
              <input type="text" name="email">
            </label>
          </div>
        </div>
      </section>
      <section>
        <div class="row">
          <label class="label col col-4">Password</label>
          <div class="col col-8">
            <label class="input"> <i class="icon-append fa fa-lock"></i>
              <input type="password" name="pass">
            </label>

          </div>
        </div>
      </section>
      <section>
        <div class="row">
          <div class="col col-4"></div>
          <div class="col col-8">
            <label class="checkbox">
              <input type="checkbox" name="remember" checked>
              <i></i>Keep me logged in</label>
          </div>
        </div>
      </section>
    </fieldset>
    <footer>
      <button type="submit" class="btn btn-default">Log in</button>
    </footer>
  </form>
</div>
<div class="copyrights"> Copyright Printiest.pk &copy 2016 </div>
