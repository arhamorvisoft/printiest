<?php

/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

?>

<?php $this->load->view('common/login-header', $header); ?>
<?php $this->load->view($body['template'], $body['data']); ?>
<?php $this->load->view('common/login-footer', $footer); ?>
