<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MX_Controller
{
/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

	function __construct()
	{
		parent::__construct();
	}

	function index(){
		if(!$this->_validate()){
			redirect('admin/index/login');
			return;
		}
		$data = array(
			'header' => array(
				'title' => 'Dashboard',
				'extras' => ''
			),
			'body' => array(
				'template' => 'dashboard/index',
				'data' => array()
			),
			'footer' => array(
				'extras' => ''
			)
		);
		$this->load->view('dashboard', $data);
	}

	function login(){
		if($this->_validate()){
			redirect('admin/index/index');
			return;
		}
		$data = array(
			'header' => array(
				'title' => 'Login',
				'extras' => ''
			),
			'body' => array(
				'template' => 'dashboard/login',
				'data' => array()
			),
			'footer' => array(
				'extras' => ''
			)
		);
		$this->load->view('login', $data);
	}

	function login_check(){
		$post = $this->input->post();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Username', 'required');
		$this->form_validation->set_rules('pass', 'Password', 'required');
		if ($this->form_validation->run() == FALSE){
			redirect('admin/index/login');
			return;
		}
		$this->load->model('admin');
		$validation = $this->admin->verifyLogin($post);
		if(count($validation) > 0){
			$validation = json_encode($validation);
			$this->session->set_userdata('admin_data', $validation);
			$this->session->set_userdata('admin_login', 1);
			redirect('admin/index/index');
		}
		redirect('admin/index/login');
		return;
	}





	function logout(){
		$this->session->unset_userdata('admin_data');
		$this->session->unset_userdata('admin_login');
		redirect('admin/index/index');
	}



	function list_orders(){

		$this->load->model('admin');
		$orders = $this->admin->orders_list();
		print_r($orders);
		exit;
		$data = array(
			'header' => array(
				'title' => 'Orders List',
				'extras' => ''
			),
			'body' => array(
				'template' => 'dashboard/list_orders',
				'data' => array()
			),
			'footer' => array(
				'extras' => ''
			)
		);
		$this->load->view('dashboard', $data);
	}


	function _validate(){
		if($this->session->userdata('admin_login')){
			return true;
		}
		return false;
	}
}
