<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ecomhut {


	/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/

	private $ci;
	function __construct(){
		$this->ci =& get_instance();
	}
	function get_admin_url($url){
		return site_url('admin/'.$url);
	}

	function get_gravatar( $email, $s = 150, $d = 'https://placehold.it/150x150', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$d = urlencode($d);
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}

	function getProfile(){
		$data = $this->ci->session->userdata('admin_data');
		$data = json_decode($data, true);
		$data['image'] = $this->get_gravatar($data['email']);
		return $data;
	}

	function getUriData($key){
		return $this->ci->frontend->getPageByKey($key);
	}

	function getChannelData($urlKey)
	{
		return $this->ci->frontend->getChannelByUrl($urlKey);
	}

	function checkLogin()
	{
		if($this->ci->session->userdata('user_login')){
			return true;
		}
		return false;
	}

	function getUserData($key=false)
	{
		if($key == false)
		{
			return $this->ci->session->userdata('user_data');
		}
		$data = $this->ci->session->userdata('user_data');
		return $data[$key];

	}

	function getCookieData($key = false)
	{
		if($key == false)
		{
			return $_COOKIE["user_data"];
		}
		$data = ($_COOKIE["user_data"]);
		print_r($data);
		exit;
		return $data[$key];
	}

	function getSettingValue($key)
	{
		$this->ci->load->model('Settingmodel');
		return $this->ci->Settingmodel->getSValues($key);
	}

	function getUserDetailById($id , $key=false)
	{
		$this->ci->load->model('users');
		return $this->ci->users->getUserById($id , $key);

	}

	function GetmetaKey()
	{
		$this->ci->load->model('Settingmodel');
		$v =$this->ci->Settingmodel->getSValues('universal_metatag');
		return $v;
	}

	function getUnreadMessage(){
		$this->ci->load->model('users');
		$user_id = $this->getUserData('user_id');
		$delete = $this->ci->users->getDeleteMessage($user_id);
		if(empty($delete)){
			$delete = array(0);
		}
		$messages = $this->ci->users->getMessagesByTo($user_id, $delete);
		$output = 0;
		$readMsg = $this->ci->users->getreadMessages($user_id);
		$finalArray = array();
		if(is_array($readMsg) && count($readMsg)){
			foreach($readMsg as $vals){
				$finalArray[] = $vals['message_id'];
			}
		}
		if(is_array($messages) && count($messages)){
			foreach($messages as $msg){
				if(!in_array($msg['id'], $finalArray)){
					$output++;
				}
			}
		}
		return $output;
	}

	public function getProfiler(){
		$this->ci->load->model('prufiler');
		if($this->ci->prufiler->checkprofiler()){
			$this->ci->output->enable_profiler(TRUE);
		}
	}
	function getChannelcomments($id)
	{
		$this->ci->load->model('Channel');
		return $this->ci->Channel->getCommentByChannelId($id);
	}

	function getvisitors($key)
	{
		$this->ci->load->model('Channel');
		return $this->ci->Channel->getvisitorsValues($key);
	}

	function user_Channel_polls($id){
		$this->ci->load->model('Users');
		$channel_data = $this->ci->Users->getChannelBychId($id);
		return $this->ci->Users->get_channel_polls($channel_data['channel_operator'] ,$channel_data['ch_id']);
	}


	function user_answersbyChannel($id , $poll_id){
		$this->ci->load->model('Users');
		$user_id = $this->getUserData('user_id');
		return  $this->ci->Users->pollsResults($id ,$poll_id);

	}

	function Chatlikes($id){
		$this->ci->load->model('Chat_model');
		return  $this->ci->Chat_model->get_chatmost_likess($id);

	}

	function exit_answersonChannel($id , $poll_id){
		$ip = $this->ci->input->ip_address();
		$this->ci->load->model('Users');
		$user_id = $this->getUserData('user_id');
		return $this->ci->Users->get_existing_answer($user_id , $id , $ip ,$poll_id);


	}

}



