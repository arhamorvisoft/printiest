<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
require_once dirname(__FILE__).'/Lang.php';
require_once dirname(__FILE__).'/Config.php';


/**
 *	@author:		OrviSoft Private Limited <info@orvisoft.com>
 *	@development:	December, 2015
 *	@for: 			Strizzle INC
 *	@repository:	http://pm.orvisoft.com/diffusion/TWITCH/
 *	@master:		https://bitbucket.org/orvisoft/twitch
 *
 **/


class CI extends CI_Controller
{
	public static $APP;
	public function __construct() {
		self::$APP = $this;
		global $LANG, $CFG;

		if ( ! $LANG instanceof MX_Lang) $LANG = new MX_Lang;
		if ( ! $CFG instanceof MX_Config) $CFG = new MX_Config;

		parent::__construct();
	}
}

new CI;
