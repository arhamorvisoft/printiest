<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function product()
	{
		$data = array(
			'header' => array(
				'pageTitle' => 'Products',
				'meta' => '',
				'bodyClass' => 'message_compose'
			),
			'footer' => array(
				'extra' => ''
			),
			'body' => '',
			'content' => array('dashboard' => ''),
			'template' => 'product/products'
		);

		$this->load->view('main' , $data);
	}

	public function document_printing()
	{
		$data = array(
			'header' => array(
				'pageTitle' => 'Document Printing',
				'meta' => '',
				'bodyClass' => 'message_compose'
			),
			'footer' => array(
				'extra' => ''
			),
			'body' => '',
			'content' => array('dashboard' => ''),
			'template' => 'product/document-printing'
		);

		$this->load->view('main' , $data);
	}

	public function document_printing_file_uploading(){
		$post = $this->input->post();
		$config['upload_path'] = './media/uploads/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if (! $this->upload->do_upload('ImageFile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$status = "error";
			$msg = "Something went wrong when saving the file, please try again.";

			//$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$status = "success";
			$msg = "File successfully uploaded";
			//$this->load->view('upload_success', $data);
		}


	echo json_encode(array('status' => $status, 'msg' => $msg));

	}

	public function project_printing()
	{
		$data = array(
			'header' => array(
				'pageTitle' => 'Project Printing',
				'meta' => '',
				'bodyClass' => 'message_compose'
			),
			'footer' => array(
				'extra' => ''
			),
			'body' => '',
			'content' => array('dashboard' => ''),
			'template' => 'product/project-printing'
		);

		$this->load->view('main' , $data);
	}

	public function letter_head()
	{
		$data = array(
			'header' => array(
				'pageTitle' => 'Letter Head',
				'meta' => '',
				'bodyClass' => 'message_compose'
			),
			'footer' => array(
				'extra' => ''
			),
			'body' => '',
			'content' => array('dashboard' => ''),
			'template' => 'product/letter-head'
		);

		$this->load->view('main' , $data);
	}

	public function photo_printing()
	{
		$data = array(
			'header' => array(
				'pageTitle' => 'Photo Printing',
				'meta' => '',
				'bodyClass' => 'message_compose'
			),
			'footer' => array(
				'extra' => ''
			),
			'body' => '',
			'content' => array('dashboard' => ''),
			'template' => 'product/photo-printing'
		);

		$this->load->view('main' , $data);
	}

	public function envelope()
	{
		$data = array(
			'header' => array(
				'pageTitle' => 'Envelope',
				'meta' => '',
				'bodyClass' => 'message_compose'
			),
			'footer' => array(
				'extra' => ''
			),
			'body' => '',
			'content' => array('dashboard' => ''),
			'template' => 'product/envelop'
		);

		$this->load->view('main' , $data);
	}

	public function visiting_card()
	{
		$data = array(
			'header' => array(
				'pageTitle' => 'Visiting Card',
				'meta' => '',
				'bodyClass' => 'message_compose'
			),
			'footer' => array(
				'extra' => ''
			),
			'body' => '',
			'content' => array('dashboard' => ''),
			'template' => 'product/visiting-card'
		);

		$this->load->view('main' , $data);
	}
}
