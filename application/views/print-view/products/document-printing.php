<div id="wrapper">
<header class="header">
<div class="container">
<div class="col-md-3 logo">
<a href="/">
<img width="200px" src="http://www.printview.in/wp-content/themes/PrintView/images/logo.png">
</a>
</div>
<div class="col-md-5">
<div class="navmenu">
<ul>
<li class="nav_active">
<a href="/products/">Products</a>
<ul>
<li>
<a href="/document-printing/">Document Printing</a>
</li>
<li>
<a href="/project-printing/">Project Printing</a>
</li>
<li>
<a href="/letter-head/">Letter Head</a>
</li>
<li>
<a href="/photo-printing/">Photo Printing</a>
</li>
<li>
<a href="/envelope/">Envelope</a>
</li>
<li>
<a href="/visiting-card/">Visiting Card</a>
</li>
</ul>
</li>
<li>
<a href="/shipping/">Shipping Info</a>
</li>
<li>
<a href="/about/">About Us</a>
<ul>
<li>
<a href="http://blog.printview.in">Blog</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
<div class="user_log col-md-4">
<div class="clearfix"></div>
</div>
</div>
</header>
<style>
<div class="content">
<div class="container">
<div class="proof-points">
<div class="breadcrumbs col-md-12">
<div class="clearfix"></div>
<div class="inner_content">
<ul class="navigationTabs">
<li class="upload col-md-3 col-sm-3 col-xs-3">
<a class="active" href="#">
<span class="text">1. Upload</span>
<span class="right active"></span>
</a>
</li>
<li id="sel2" class="paper col-md-3 col-sm-3 col-xs-3">
<a class="inactive" href="#">
<span class="left"></span>
<span class="text">2. Paper</span>
<span class="right"></span>
</a>
</li>
<li id="sel3" class="paper col-md-3 col-sm-3 col-xs-3">
<a class="inactive" href="#">
<span class="left"></span>
<span class="text">3. Binding</span>
<span class="right"></span>
</a>
</li>
<li id="sel4" class="paper col-md-3 col-sm-3 col-xs-3">
<a class="inactive" href="#">
<span class="left"></span>
<span class="text">4. Order</span>
<span class="right"></span>
</a>
</li>
<span class="clearfix"></span>
</ul>
<div class="col-md-1"> </div>
<div class="col-md-5">
<h1>Document Printing</h1>
<span class="delivery_span">
<b>Free shipping for all orders above Rs. 50/-</b>
<br>
* Large volume orders may take longer. Read more about our
<a href="/shipping/">Shipping Policy</a>
</span>
<div id="doc_viewer2" class="doc_viewer2"></div>
<div id="doc_viewer" class="doc_viewer" style="display:none;"></div>
</div>
<div class="col-md-5 preview_side">
<div id="fk-alert"></div>
<div id="step_1">
<div class="step_1">
<div id="fk-alert"></div>
<div class="panel panel-primary">
<div class="panel-heading">
<h3 class="panel-title">Upload</h3>
</div>
<div class="panel-body">
<div id="upload-wrapper" align="center">
<div id="send-email">
<input id="send_later" class="css-checkbox" type="checkbox">
<label class="css-label" name="send_later" for="send_later">My file is larger than 12MB. I will email it to sales@printview.in</label>
</div>
<br>
<div id="upload-section">
<span class="notice">Allowed File Types: PDF. | Maximum Size : 12 MB.</span>
<span class="notice">
Tip :
<a target="_blank" rel="nofollow" href="http://www.freepdfconvert.com/">DOC/DOCX to PDF Conversion</a>
</span>
<form id="MyUploadForm" data-ajax="false" enctype="multipart/form-data" method="post" onsubmit="return false" action="http://www.printview.in/wp-content/themes/PrintView/processupload.php">
<div id="file_up">
<input id="imageInput" type="file" name="ImageFile">
<input id="submit-btn" type="submit" value="Upload" name="submit-btn">
<img id="loading-img" alt="Please Wait" style="display:none;" src="http://www.printview.in/wp-content/themes/PrintView/images/ajax-loader.gif">
</div>
<div id="progressboxs" style="display:none;">
<div id="progressbar"></div>
<div id="statustxt">0%</div>
</div>
<div id="countdowntimer" style="display:none; margin-top:10px;">
<input id="restTime" type="text" style="display: none" value="180">
<div id="progressTimer"></div>
</div>
<div id="output">
<input id="file_loc" type="hidden" value="" name="file_loc">
</div>
</form>
</div>
<div id="file_pages"> </div>
<div class="clearfix"></div>
<p style="text-align:center; font-style:italic;">
<strong>If the Page alignment Of your ".doc" file changes Please Save them as Pdf and then upload. </strong>
Don't Know how To convert ?
<a style="color:#20ACFB;" target="_blank" href="http://blog.printview.in/how-to-convert-a-word-excel-or-powerpoint-file-to-pdf-using-microsoft-office/">Visit Blog</a>
</p>
<p style="text-align:center; font-style:italic;">
<strong>NOTE :</strong>
File uploads can take upto 3-5 minutes. Please be patient while file upload is in progress.
</p>
<p style="text-align:center; font-style:italic;">
<strong>NOTE :</strong>
Orders with invalid or incorrect data may lead to order cancellation.
</p>
<br>
<a id="goto-2" class="btn btn-block btn-success">Next</a>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div id="step_2" style="display:none;">
<div class="step_2">
<div class="panel panel-primary">
<div class="panel-heading">
<h3 class="panel-title">Type</h3>
</div>
<div class="panel-body text-center">
<div>
<input id="option2" type="radio" checked="" style="display: none" value="BW" name="options">
<label for="option2">
<div class="color select">
<i>Black & White</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/print-black.jpg">
<i>Rs. 1.50 /page</i>
</div>
</label>
<input id="option1" type="radio" style="display: none" value="COLOR" name="options">
<label for="option1">
<div class="color select">
<i>Colour</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/print-color.jpg">
<i>Rs. 4.00 /page</i>
</div>
</label>
</div>
<br>
<a id="goto-3" class="btn btn-block btn-success">Next</a>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div id="step_3" class="" style="display:none;">
<div class="step_3">
<div class="panel panel-primary">
<div class="panel-heading">
<h3 class="panel-title">Paper Type</h3>
</div>
<div class="panel-body text-center">
<div id="paper_color" style="display:none;">
<input id="p_type_color0" type="radio" checked="" style="display: none" value="75 GSM(Normal)_0" name="p_type_color">
<label for="p_type_color0">
<div class="color select">
<i>75 GSM(Normal)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Included </i>
</div>
</label>
<input id="p_type_color1" type="radio" style="display: none" value="100 GSM (JK Cedar)_0.2" name="p_type_color">
<label for="p_type_color1">
<div class="color select">
<i>100 GSM (JK Cedar)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.2 /page</i>
</div>
</label>
<input id="p_type_color2" type="radio" style="display: none" value="80 GSM(Excel Bond)_0.5" name="p_type_color">
<label for="p_type_color2">
<div class="color select">
<i>80 GSM(Excel Bond)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.5 /page</i>
</div>
</label>
<input id="p_type_color3" type="radio" style="display: none" value="90 GSM(Excel Bond)_0.6" name="p_type_color">
<label for="p_type_color3">
<div class="color select">
<i>90 GSM(Excel Bond)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.6 /page</i>
</div>
</label>
<input id="p_type_color4" type="radio" style="display: none" value="100 GSM (Royal Executive Bond)_0.6" name="p_type_color">
<label for="p_type_color4">
<div class="color select">
<i>100 GSM (Royal Executive Bond)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.6 /page</i>
</div>
</label>
<input id="p_type_color5" type="radio" style="display: none" value="180 GSM Premium (Gloss Coated)_8" name="p_type_color">
<label for="p_type_color5">
<div class="color select">
<i>180 GSM Premium (Gloss Coated)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 8 /page</i>
</div>
</label>
</div>
<div id="paper_bw" style="display:none;">
<input id="p_type_bw0" type="radio" style="display: none" checked="" value="75 GSM (Normal)_0" name="p_type_bw">
<label for="p_type_bw0">
<div class="color select">
<i>75 GSM (Normal)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Included </i>
</div>
</label>
<input id="p_type_bw1" type="radio" style="display: none" value="100 GSM (JK Cedar)_0.2" name="p_type_bw">
<label for="p_type_bw1">
<div class="color select">
<i>100 GSM (JK Cedar)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.2 /page</i>
</div>
</label>
<input id="p_type_bw2" type="radio" style="display: none" value="80 GSM (Excel Bond)_0.5" name="p_type_bw">
<label for="p_type_bw2">
<div class="color select">
<i>80 GSM (Excel Bond)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.5 /page</i>
</div>
</label>
<input id="p_type_bw3" type="radio" style="display: none" value="90 GSM (Excel Bond)_0.6" name="p_type_bw">
<label for="p_type_bw3">
<div class="color select">
<i>90 GSM (Excel Bond)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.6 /page</i>
</div>
</label>
<input id="p_type_bw4" type="radio" style="display: none" value="100 GSM (Royal Executive Bond)_0.6" name="p_type_bw">
<label for="p_type_bw4">
<div class="color select">
<i>100 GSM (Royal Executive Bond)</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
<i>Rs 0.6 /page</i>
</div>
</label>
</div>
<br>
<a id="goto-4" class="btn btn-block btn-success">Next</a>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div id="step_5" class="" style="display:none;">
<div class="step_3">
<div class="panel panel-primary">
<div class="panel-heading">
<h3 class="panel-title">Binding</h3>
</div>
<div class="panel-body text-center">
<div>
<input id="b_option1" type="radio" checked="" style="display: none" value="none" name="b_options">
<label for="b_option1">
<div class="color select">
<i>No Binding</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/no-binding.jpg">
<i>included in price</i>
</div>
</label>
<input id="b_option2" type="radio" style="display: none" value="spiral" name="b_options">
<label for="b_option2">
<div class="color select">
<i>Spiral Binding</i>
<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/spiral-binding.png">
<i>Rs. 25/-</i>
</div>
</label>
</div>
<br>
<a id="goto-5" class="btn btn-block btn-success">Next</a>
</div>
</div>
<br>
</div>
</div>
<div class="clearfix"></div>
<div id="step_4" class="" style="display:none;">
<div class="step_4">
<div class="panel panel-primary">
<div class="panel-heading">
<h3 class="panel-title">No. of Copies</h3>
</div>
<div class="panel-body">
<div class="col-md-6">
<div class="item_count">
<div class="form-group">
<label for="">Number of Copies :</label>
<input id="c_count" class="form-control" type="text" value="1" placeholder="Number of Copies" name="c_count">
</div>
</div>
</div>
<div class="col-md-6" style="padding-top: 20px;">
<span class="cross_X">X</span>
<span id="count_items" class="count_items">-</span>
</div>
<div class="clearfix"></div>
<div class="price_box">
<span>Total Cost</span>
<h4 id="total_price_h4">-</h4>
</div>
<div class="clearfix"></div>
<a id="goto-6" class="btn btn-block btn-success">Calculate Total Charges</a>
<br>
<a id="step_6" class="btn btn-block btn-info" style="display:none;">Confirm and Place Order</a>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div>
<h3 style="color: #000;text-align: center;font-style: italic;">Order Overview :</h3>
<table id="newspaper-a" summary="Document Printing Order Overview">
<tbody>
<tr>
<td>Item</td>
<td>Quality</td>
<td>Price</td>
<td>Sub-Total</td>
</tr>
<tr>
<td>Type</td>
<td>Document</td>
<td>-</td>
<td id="page_total">-</td>
</tr>
<tr>
<td>Printing</td>
<td id="print_type">-</td>
<td id="print_type_price">-</td>
<td id="print_type_tprice">-</td>
</tr>
<tr>
<td>Paper Quality</td>
<td id="paper_type">-</td>
<td id="paper_type_price">-</td>
<td id="paper_type_tprice">-</td>
</tr>
<tr>
<td>Binding</td>
<td id="binding_type">-</td>
<td id="binding_type_price">-</td>
<td id="binding_type_tprice">-</td>
</tr>
<tr>
<td>Copies</td>
<td>-</td>
<td id="copies_number">-</td>
<td id="copies_price">-</td>
</tr>
<tr>
<td style="background-color:#A4D483;">-</td>
<td style="background-color:#A4D483;">-</td>
<td style="background-color:#A4D483;">
<b>Total : </b>
</td>
<td id="total_price" style="background-color:#A4D483;">-</td>
</tr>
</tbody>
</table>
</div>
<div class="clearfix"></div>
<div> </div>
</div>
<div class="col-md-1"> </div>
<div class="clearfix"></div>
<div class="col-md-7 descript">
<h3>Document Printing</h3>
<p>We have service for document printing with high resolution colour and B&W.  We offer quality printing on time. Above document printing we also have other services include project printing, letter head, photo printing, envelop printing and visiting card printing.</p>
</div>
<div class="col-md-5 image_cont text-center">
<br>
<img class="attachment-post-thumbnail wp-post-image" width="200" height="225" alt="Document Printing Services India" data-lazy-src="http://www.printview.in/wp-content/uploads/2014/07/document-printing1.jpg" src="http://www.printview.in/wp-content/plugins/lazy-load/images/1x1.trans.gif">
<noscript><img width="200" height="225" src="http://www.printview.in/wp-content/uploads/2014/07/document-printing1.jpg" class="attachment-post-thumbnail wp-post-image" alt="Document Printing Services India" /></noscript>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="push"></div>
</div>
</div>
</div>