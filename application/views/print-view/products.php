<div id="wrapper">
<header class="header">
<div class="container">
<div class="col-md-3 logo">
<a href="/">
<img width="200px" src="http://www.printview.in/wp-content/themes/PrintView/images/logo.png">
</a>
</div>
<div class="col-md-5">
<div class="navmenu">
<ul>
<li class="nav_active">
<a href="/products/">Products</a>
<ul>
<li>
<a href="/document-printing/">Document Printing</a>
</li>
<li>
<a href="/project-printing/">Project Printing</a>
</li>
<li>
<a href="/letter-head/">Letter Head</a>
</li>
<li>
<a href="/photo-printing/">Photo Printing</a>
</li>
<li>
<a href="/envelope/">Envelope</a>
</li>
<li>
<a href="/visiting-card/">Visiting Card</a>
</li>
</ul>
</li>
<li>
<a href="/shipping/">Shipping Info</a>
</li>
<li>
<a href="/about/">About Us</a>
<ul>
<li>
<a href="http://blog.printview.in">Blog</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
<div class="user_log col-md-4">
<span class="top_link tl_login">
<a href="/login/">Login</a>
</span>
<span class="top_link tl_reg">
<a href="/register/">Register</a>
</span>
<span class="top_link tl_cart">
<span class="dropdown">
<a class="dropdown-toggle" href="/cart/" data-toggle="dropdown">
My Cart [ 0 ]
<span class="caret"></span>
</a>
<ul class="dropdown-menu">
<li>
<span class="cart_items">
<a href="/products/">Your cart is empty</a>
</span>
</li>
<li style="border-top:1px solid;">
<span class="cart_items">
<a href="/products/">Grand Total : </a>
<b>Rs. 0.00/-</b>
</span>
</li>
<li class="cart_check">
<a class="btn btn-success" href="/products/">Add Items</a>
</li>
</ul>
</span>
</span>
</div>
<div class="clearfix"></div>
</div>
</header>
<div class="content">
<div class="container">
<div class="col-md-4 col-sm-4">
<a href="/document-printing/">
<div class="block block_black">
<h2>DOCUMENT PRINTING</h2>
<h3>@ Rs. 1.50/- onwards</h3>
<div class="block_hover">
<p class="btn btn-primary btn-sm">Order Now!</p>
</div>
</div>
</a>
</div>
<div class="col-md-4 col-sm-4">
<a href="/project-printing/">
<div class="block block_pink">
<h2>PROJECT PRINTING</h2>
<h3>@ Rs. 1.50/- onwards</h3>
<div class="block_hover">
<p class="btn btn-primary btn-sm">Order Now!</p>
</div>
</div>
</a>
</div>
<div class="col-md-4 col-sm-4">
<a href="/letter-head/">
<div class="block block_yellow">
<h2>LETTER HEAD</h2>
<h3>@ Rs. 3.50/- onwards</h3>
<div class="block_hover">
<p class="btn btn-primary btn-sm">Order Now!</p>
</div>
</div>
</a>
</div>
</div>
<div class="container">
<div class="col-md-4 col-sm-4">
<a href="/photo-printing/">
<div class="block block_green">
<h2>PHOTO PRINTING</h2>
<h3>@ Re. 1.00/- onwards</h3>
<div class="block_hover">
<p class="btn btn-primary btn-sm">Order Now!</p>
</div>
</div>
</a>
</div>
<div class="col-md-4 col-sm-4">
<a href="/envelope/">
<div class="block block_violet">
<h2>ENVELOPE PRINTING</h2>
<h3>@ Rs. 2.5/- onwards</h3>
<div class="block_hover">
<p class="btn btn-primary btn-sm">Order Now!</p>
</div>
</div>
</a>
</div>
<div class="col-md-4 col-sm-4">
<a href="/visiting-card/">
<div class="block block_brown">
<h2>VISITING CARD</h2>
<h3>@ Rs. 1/- onwards</h3>
<div class="block_hover">
<p class="btn btn-primary btn-sm">Order Now!</p>
</div>
</div>
</a>
</div>
</div>
</div>
<div class="push"></div>
</div>