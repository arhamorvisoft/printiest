<div id="wrapper">
<header class="header">
<div class="container">
<div class="col-md-3 logo">
<a href="/">
<img width="200px" src="<?php echo base_url('assets/images/logo.png'); ?>">
</a>
</div>
<div class="col-md-5">
<div class="navmenu">
<ul>
<li>
<a href="<?php echo site_url('Products/products'); ?>">Products</a>
<ul>
<li>
<a href="<?php echo site_url('Products/document_printing'); ?>">Document Printing</a>
</li>
<li>
<a href="<?php echo site_url('Products/project_printing'); ?>">Project Printing</a>
</li>
<li>
<a href="<?php echo site_url('Products/letter_head'); ?>">Letter Head</a>
</li>
<li>
<a href="<?php echo site_url('Products/photo_printing'); ?>">Photo Printing</a>
</li>
<li>
<a href="<?php echo site_url('Products/envelope'); ?>">Envelope</a>
</li>
<li>
<a href="<?php echo site_url('Products/visiting_card'); ?>">Visiting Card</a>
</li>
</ul>
</li>
<li>
<a href="<?php echo site_url('welcome/shipping'); ?>">Shipping Info</a>
</li>
<li class="nav_active">
<a href="<?php echo site_url('welcome/about'); ?>">About Us</a>
<ul>
<li>
<a href="#">Blog</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
<div class="user_log col-md-4">
<span class="top_link tl_login">
<a href="<?php echo site_url('welcome/login'); ?>">Login</a>
</span>
<span class="top_link tl_reg">
<a href="<?php echo site_url('welcome/register'); ?>">Register</a>
</span>
<span class="top_link tl_cart">
<span class="dropdown">
<a class="dropdown-toggle" href="<?php echo site_url('welcome/cart'); ?>" data-toggle="dropdown">
My Cart [ 0 ]
<span class="caret"></span>
</a>
<ul class="dropdown-menu">
<li>
<span class="cart_items">
<a href="<?php echo site_url('Products/products'); ?>">Your cart is empty</a>
</span>
</li>
<li style="border-top:1px solid;">
<span class="cart_items">
<a href="<?php echo site_url('Products/products'); ?>">Grand Total : </a>
<b>Rs. 0.00/-</b>
</span>
</li>
<li class="cart_check">
<a class="btn btn-success" href="<?php echo site_url('Products/products'); ?>">Add Items</a>
</li>
</ul>
</span>
</span>
</div>
<div class="clearfix"></div>
</div>
</header>
<div class="content">
<div class="container">
<br>
<br>
<div class="inner_content print_tables">
<div class="col-md-9 page_content">
<h1> About </h1>
<h2></h2>
<p>Hello!</p>
<p>
It’s nice of you to show interest in us. Here are few things you might like to know.
<br>
Printview started its operation in 2013 with the objective of making affordable printing in Bhubaneswar. Then we realised we need a platform where we can take order easily from anywhere and can provide better service. Although we have very few service at this point of time but we believe that we have delivered a good service quality to our customers.
</p>
<p>Our service includes Document Printing, Letterhead Printing, Envelop Printing, Self-Customized business card and corporate printing solutions.</p>
<p> </p>
<p>If you have any queries/Need any information mail us support@printview.in.</p>
<p> </p>
</div>
<div class="col-md-3">
<div class="panel panel-info sidebox">
<div class="panel-body">
<span class="side_cont">
<strong style="padding-right:18px">Phone </strong>
:     
<span class="skype_c2c_print_container notranslate">+91-8213954606</span>
<span id="skype_c2c_container" class="skype_c2c_container notranslate" data-ismobile="false" data-isrtl="false" data-isfreecall="false" data-numbertype="paid" data-numbertocall="+918213954606" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" tabindex="-1" dir="ltr">
<span class="skype_c2c_highlighting_inactive_common" skypeaction="skype_dropdown" dir="ltr">
<span id="non_free_num_ui" class="skype_c2c_textarea_span">
<img class="skype_c2c_logo_img" width="0" height="0" src="resource://skype_ff_extension-at-jetpack/skype_ff_extension/data/call_skype_logo.png">
<span class="skype_c2c_text_span">+91-8213954606</span>
<span class="skype_c2c_free_text_span"></span>
</span>
</span>
</span>
</span>
<span class="side_cont">
<strong style="padding-right:24px">Email </strong>
:     sales@printview.in
</span>
<br>
<span class="side_cont">
<strong>Address  </strong>
:   PrintView Services Pvt. Ltd. , Plot No 1182/3294, Jaganmohan Nagar, Khandagiri, Bhubaneswar.
</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="push"></div>
</div>