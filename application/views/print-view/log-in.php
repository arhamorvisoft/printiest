<div id="wrapper">
<header class="header">
<div class="content">
<div class="container">
<br>
<br>
<div class="inner_content form_contnt">
<div class="col-sm-offset-3 col-md-4 col-sm-6 print_form">
<h2 class="text-center">Login Now</h2>
<div id="login_loading" class="login_loading" style="display:none;">
<img alt="Loading ..." src="http://www.printview.in/wp-content/themes/PrintView/images/loading.gif">
</div>
<div id="login_done" class="login_done" style="display:none;"></div>
<form id="login_form" class="form-horizontal" action="/login/" method="post" name="login_form" role="form" novalidate="novalidate">
<div class="form-group">
<input id="reg_email" class="form-control" type="email" required="" placeholder="Enter your Email" name="reg_email" aria-required="true" style="background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAALZJREFUOBFjYKAANDQ0rGWiQD9IqzgL0BQ3IKMXiB8AcSKQ/waIrYDsKUD8Fir2pKmpSf/fv3+zgPxfzMzMSbW1tbeBbAaQC+b+//9fB4h9gOwikCAQTAPyDYHYBciuBQkANfcB+WZAbPP37992kBgIUOoFBiZGRsYkIL4ExJvZ2NhAXmFgYmLKBPLPAfFuFhaWJpAYEBQC+SeA+BDQC5UQIQpJYFgdodQLLyh0w6j20RCgUggAAEREPpKMfaEsAAAAAElFTkSuQmCC"); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
</div>
<div class="form-group">
<input id="reg_pass" class="form-control" type="password" required="" placeholder="Enter your password" name="reg_pass" aria-required="true" style="background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAALZJREFUOBFjYKAANDQ0rGWiQD9IqzgL0BQ3IKMXiB8AcSKQ/waIrYDsKUD8Fir2pKmpSf/fv3+zgPxfzMzMSbW1tbeBbAaQC+b+//9fB4h9gOwikCAQTAPyDYHYBciuBQkANfcB+WZAbPP37992kBgIUOoFBiZGRsYkIL4ExJvZ2NhAXmFgYmLKBPLPAfFuFhaWJpAYEBQC+SeA+BDQC5UQIQpJYFgdodQLLyh0w6j20RCgUggAAEREPpKMfaEsAAAAAElFTkSuQmCC"); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
</div>
<div class="form-group">
<button class="btn btn-primary btn-block" type="submit">Sign in</button>
<a class="btn btn-primary btn-block" href="/password-recovery/">Forgot Password?</a>
</div>
</form>
</div>
<div class="col-md-3 col-sm-12 login_social">
<span class="divider"></span>
<a class="facebook_button" href="http://www.printview.in/fb/loginFB.php">Sign In With Facebook</a>
<a class="gplus_button" href="http://www.printview.in/google/google.php">Sign In With Google</a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="push"></div>
</div>