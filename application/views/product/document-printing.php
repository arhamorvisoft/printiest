<div class="content">
	<div class="container">
		<div class="proof-points">
			<ul>

				<li class="proof_click"><a href="#">Online Printing Services</a></li>

				<li class="proof_del"><a href="#">Cash on Delivery</a></li>

				<li class="proof_sameday"><a href="#">8 Hour Delivery</a></li>

			</ul>

		</div>

		<div class="breadcrumbs col-md-12">

			<ul class="col-md-9">
				<li><a href="/" title="Go to Home Page">Home</a></li>

				<span class="breadcrumb-divider"></span>

				<li><a href="<?php echo site_url('Products/product'); ?>">Printing Services</a></li>

				<span class="breadcrumb-divider"></span>

				<li>Document Printing</li>
			</ul>

		</div>

		<div class="clearfix"></div>

		<div class="inner_content">

			<ul class="navigationTabs">

				<li class="upload col-md-3 col-sm-3 col-xs-3">

					<a href="#" class="active"><span class="text">1. Upload</span><span class="right active"></span></a>

				</li>

				<li class="paper col-md-3 col-sm-3 col-xs-3" id="sel2">

					<a href="#" class="inactive"><span class="left"></span><span class="text">2. Paper</span><span class="right"></span></a>

				</li>

				<li class="paper col-md-3 col-sm-3 col-xs-3" id="sel3">

					<a href="#" class="inactive"><span class="left"></span><span class="text">3. Binding</span><span class="right"></span></a>

				</li>

				<li class="paper col-md-3 col-sm-3 col-xs-3" id="sel4">

					<a href="#" class="inactive"><span class="left"></span><span class="text">4. Order</span><span class="right"></span></a>

				</li>

				<span class="clearfix"></span>

			</ul>

			<div class="col-md-1">

			</div>

			<div class="col-md-5">

				<h1>Document Printing</h1>

				<span class="delivery_span"><b>Free shipping for all orders above Rs. 50/-</b>

					<br>* Large volume orders may take longer. Read more about our <a href="/shipping/">Shipping Policy</a></span>


				<div class="doc_viewer2" id="doc_viewer2"></div>

				<div class="doc_viewer" id="doc_viewer" style="display:none;"></div>

			</div>



			<div class="col-md-5 preview_side">

				<div id="fk-alert"></div>

				<div id="step_1">

					<div class="step_1">

						<div id="fk-alert"></div>

						<div class="panel panel-primary">

							<div class="panel-heading">

								<h3 class="panel-title">Upload</h3>

							</div>

							<div class="panel-body">

								<div id="upload-wrapper" align="center">

									<div id="send-email">

										<input id="send_later" class="css-checkbox" type="checkbox">

										<label for="send_later" name="send_later" class="css-label">My file is larger than 12MB. I will email it to sales@printview.in</label>

									</div>

									<br>

									<div id="upload-section">


										<span class="notice">Allowed File Types: PDF. | Maximum Size : 12 MB.</span>
										<span class="notice">Tip : <a href="http://www.freepdfconvert.com/" rel="nofollow" target="_blank">DOC/DOCX to PDF Conversion</a></span>

										<form id="uploadingForm" data-ajax="false" enctype="multipart/form-data" method="post" onsubmit="return false" action="#">

											<div id="file_up">

												<input name="ImageFile" id="imageInput" type="file">

												<input id="upload" name="submit-btn" value="Upload" type="submit">

												<img src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>" id="loading-img" style="display:none;" alt="Please Wait">

											</div>

											<div id="progressboxs" style="display:none;"><div id="progressbar"></div><div id="statustxt">0%</div></div>
											<div id="countdowntimer" style="display:none; margin-top:10px;"><input id="restTime" value="180" style="display: none" type="text">
												<div id="progressTimer"></div></div>

											<div id="output"><input name="file_loc" id="file_loc" value="" type="hidden"></div>

										</form>


									</div>
									<script>

										$("#upload").on("click", function() {
											var file_data = $("#imageInput").prop("files")[0];
											var form_data = new FormData();
											form_data.append("file", file_data)
											alert(form_data);
											$.ajax({
												url: "<?php echo base_url('index.php/Products/document_printing_file_uploading'); ?>",
												dataType: 'script',
												cache: false,
												contentType: false,
												processData: false,
												data: form_data,
												type: 'post',
												success: function(){
													alert("works");
												}
											});
										});
									</script>
									<div id="file_pages"> </div>

									<div class="clearfix"></div>

									<p style="text-align:center; font-style:italic;"><strong>If the Page alignment Of your ".doc" file changes Please Save them as Pdf and then upload. </strong>Don't Know how To convert ? <a href="http://blog.printview.in/how-to-convert-a-word-excel-or-powerpoint-file-to-pdf-using-microsoft-office/" target="_blank" style="color:#20ACFB;">Visit Blog</a> </p>

									<p style="text-align:center; font-style:italic;"><strong>NOTE :</strong> File uploads can take upto 3-5 minutes. Please be patient while file upload is in progress. </p>

									<p style="text-align:center; font-style:italic;"><strong>NOTE :</strong> Orders with invalid or incorrect data may lead to order cancellation. </p>

									<br>

									<a class="btn btn-block btn-success" id="goto-2">Next</a>

									<div class="clearfix"></div>

								</div>

							</div>

						</div>

					</div>

				</div>

				<div class="clearfix"></div>

				<div id="step_2" style="display:none;">

					<div class="step_2">

						<div class="panel panel-primary">

							<div class="panel-heading"><h3 class="panel-title">Type</h3></div>

							<div class="panel-body text-center">

								<div>

									<input name="options" id="option2" value="BW" style="display: none" checked="" type="radio">

									<label for="option2">

										<div class="color select">

											<i>Black &amp; White</i>

											<img src="<?php echo base_url('assets/images/print-black.jpg');?>" height="100%" width="100%">

											<i>Rs. 1.50 /page</i>

										</div>

									</label>

									<input name="options" id="option1" value="COLOR" style="display: none" type="radio">

									<label for="option1">

										<div class="color select">

											<i>Colour</i>

											<img src="<?php echo base_url('assets/images/print-color.jpg'); ?>" height="100%" width="100%">

											<i>Rs. 4.00 /page</i>

										</div>

									</label>

								</div><br>

								<a class="btn btn-block btn-success" id="goto-3">Next</a>



							</div>





						</div>

					</div>

				</div>

				<div class="clearfix"></div>

				<div class="" id="step_3" style="display:none;">

					<div class="step_3">

						<div class="panel panel-primary">

							<div class="panel-heading">

								<h3 class="panel-title">Paper Type</h3>

							</div>

							<div class="panel-body text-center">

								<div id="paper_color" style="display:none;">



									<input name="p_type_color" id="p_type_color0" value="75 GSM(Normal)_0" style="display: none" checked="" type="radio">

									<label for="p_type_color0"> <div class="color select">

										<i>75 GSM(Normal)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Included </i>

										</div></label>



									<input name="p_type_color" id="p_type_color1" value="100 GSM (JK Cedar)_0.2" style="display: none" type="radio">

									<label for="p_type_color1"> <div class="color select">

										<i>100 GSM (JK Cedar)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.2 /page</i>

										</div></label>



									<input name="p_type_color" id="p_type_color2" value="80 GSM(Excel Bond)_0.5" style="display: none" type="radio">

									<label for="p_type_color2"> <div class="color select">

										<i>80 GSM(Excel Bond)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.5 /page</i>

										</div></label>



									<input name="p_type_color" id="p_type_color3" value="90 GSM(Excel Bond)_0.6" style="display: none" type="radio">

									<label for="p_type_color3"> <div class="color select">

										<i>90 GSM(Excel Bond)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.6 /page</i>

										</div></label>



									<input name="p_type_color" id="p_type_color4" value="100 GSM (Royal Executive Bond)_0.6" style="display: none" type="radio">

									<label for="p_type_color4"> <div class="color select">

										<i>100 GSM (Royal Executive Bond)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.6 /page</i>

										</div></label>



									<input name="p_type_color" id="p_type_color5" value="180 GSM Premium (Gloss Coated)_8" style="display: none" type="radio">

									<label for="p_type_color5"> <div class="color select">

										<i>180 GSM Premium (Gloss Coated)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 8 /page</i>

										</div></label>


								</div>

								<div id="paper_bw" style="display:none;">



									<input name="p_type_bw" id="p_type_bw0" value="75 GSM (Normal)_0" checked="" style="display: none" type="radio">

									<label for="p_type_bw0"><div class="color select">

										<i>75 GSM (Normal)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Included </i>

										</div></label>



									<input name="p_type_bw" id="p_type_bw1" value="100 GSM (JK Cedar)_0.2" style="display: none" type="radio">

									<label for="p_type_bw1"><div class="color select">

										<i>100 GSM (JK Cedar)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.2 /page</i>

										</div></label>



									<input name="p_type_bw" id="p_type_bw2" value="80 GSM (Excel Bond)_0.5" style="display: none" type="radio">

									<label for="p_type_bw2"><div class="color select">

										<i>80 GSM (Excel Bond)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.5 /page</i>

										</div></label>



									<input name="p_type_bw" id="p_type_bw3" value="90 GSM (Excel Bond)_0.6" style="display: none" type="radio">

									<label for="p_type_bw3"><div class="color select">

										<i>90 GSM (Excel Bond)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.6 /page</i>

										</div></label>



									<input name="p_type_bw" id="p_type_bw4" value="100 GSM (Royal Executive Bond)_0.6" style="display: none" type="radio">

									<label for="p_type_bw4"><div class="color select">

										<i>100 GSM (Royal Executive Bond)</i>

										<img src="<?php echo base_url('assets/images/paper.jpg'); ?>" height="100%" width="100%">

										<i>Rs 0.6 /page</i>

										</div></label>


								</div><br>

								<a class="btn btn-block btn-success" id="goto-4">Next</a>

							</div>

						</div>

					</div>

				</div>

				<div class="clearfix"></div>

				<div class="" id="step_5" style="display:none;">

					<div class="step_3">



						<div class="panel panel-primary">

							<div class="panel-heading">

								<h3 class="panel-title">Binding</h3>

							</div>

							<div class="panel-body text-center">

								<div>

									<input name="b_options" id="b_option1" value="none" style="display: none" checked="" type="radio">

									<label for="b_option1">

										<div class="color select">

											<i>No Binding</i>



											<img src="<?php echo base_url('assets/images/no-binding.jpg'); ?>" height="100%" width="100%">





											<i>included in price</i>

										</div>

									</label>

									<input name="b_options" id="b_option2" value="spiral" style="display: none" type="radio">

									<label for="b_option2">

										<div class="color select">

											<i>Spiral Binding</i>



											<img src="<?php echo base_url('assets/images/spiral-binding.png');?>" height="100%" width="100%">





											<i>Rs. 25/-</i>

										</div>

									</label>

								</div><br>

								<a class="btn btn-block btn-success" id="goto-5">Next</a>

							</div>

						</div>

						<br>

					</div>

				</div>

				<div class="clearfix"></div>

				<div class="" id="step_4" style="display:none;">

					<div class="step_4">

						<div class="panel panel-primary">

							<div class="panel-heading">

								<h3 class="panel-title">No. of Copies</h3>

							</div>

							<div class="panel-body">

								<div class="col-md-6">

									<div class="item_count">

										<div class="form-group">

											<label for="">Number of Copies :</label>

											<input class="form-control" name="c_count" id="c_count" placeholder="Number of Copies" value="1" type="text">

										</div>



									</div>

								</div>

								<div class="col-md-6" style="padding-top: 20px;">

									<span class="cross_X">X</span>

									<span class="count_items" id="count_items">-</span>

								</div>

								<div class="clearfix"></div>

								<div class="price_box">

									<span>Total Cost</span>

									<h4 id="total_price_h4">-</h4>

								</div>

								<div class="clearfix"></div>

								<a class="btn btn-block btn-success" id="goto-6">Calculate Total Charges</a><br>



								<a class="btn btn-block btn-info" id="step_6" style="display:none;">Confirm and Place Order</a>

							</div>

						</div>

					</div>

				</div>

				<div class="clearfix"></div>



				<div>

					<h3 style="color: #000;text-align: center;font-style: italic;">Order Overview :</h3>

					<table id="newspaper-a" summary="Document Printing Order Overview">



						<tbody>

							<tr>

								<td>Item</td>

								<td>Quality</td>

								<td>Price</td>

								<td>Sub-Total</td>

							</tr>



							<tr>

								<td>Type</td>

								<td>Document</td>

								<td>-</td>

								<td id="page_total">-</td>

							</tr>



							<tr>

								<td>Printing</td>

								<td id="print_type">-</td>

								<td id="print_type_price">-</td>

								<td id="print_type_tprice">-</td>

							</tr>



							<tr>

								<td>Paper Quality</td>

								<td id="paper_type">-</td>

								<td id="paper_type_price">-</td>

								<td id="paper_type_tprice">-</td>

							</tr>



							<tr>

								<td>Binding</td>

								<td id="binding_type">-</td>

								<td id="binding_type_price">-</td>

								<td id="binding_type_tprice">-</td>



							</tr>



							<tr>

								<td>Copies</td>

								<td>-</td>

								<td id="copies_number">-</td>

								<td id="copies_price">-</td>

							</tr>



							<tr>

								<td style="background-color:#A4D483;">-</td>

								<td style="background-color:#A4D483;">-</td>

								<td style="background-color:#A4D483;"><b>Total : </b></td>

								<td style="background-color:#A4D483;" id="total_price">-</td>

							</tr>



						</tbody>

					</table>

				</div>

				<div class="clearfix"></div>

				<div>

				</div>

			</div>

			<div class="col-md-1">

			</div>

			<div class="clearfix"></div>



			<div class="col-md-7 descript">

				<h3>Document Printing</h3>

				<p>We have service for document printing with high resolution colour and B&amp;W.&nbsp; We offer quality printing on time. Above document printing we also have other services include project printing, letter head, photo printing, envelop printing and visiting card printing.</p>

			</div>

			<div class="col-md-5 image_cont text-center"><br>

				<img src="<?php echo base_url('assets/images/1x1.trans.gif'); ?>" data-lazy-src="<?php echo base_url('assets/images/document-printing1.jpg'); ?>" class="attachment-post-thumbnail wp-post-image" alt="Document Printing Services Pakistan" height="225" width="200"><noscript><img width="200" height="225" src="<?php echo base_url('assets/images/document-printing1.jpg'); ?>" class="attachment-post-thumbnail wp-post-image" alt="Document Printing Services Pakistan" /></noscript>
			</div>


			<div class="clearfix"></div>

		</div>

	</div>

</div>

<div class="push"></div>

