<div class="content">

	<div class="container">

		<div class="proof-points">

			<ul>

				<li class="proof_click"><a href="#">Online Printing Services</a></li>

				<li class="proof_del"><a href="#">Cash on Delivery</a></li>

				<li class="proof_sameday"><a href="#">8 Hour Delivery</a></li>

			</ul>

		</div>

	<div class="breadcrumbs col-md-12">

		<ul class="col-md-9">

			<li><a title="Go to Home Page" href="<?php echo site_url('Welcome/index'); ?>">Home</a></li>

			<span class="breadcrumb-divider"></span>

			<li><a href="/products/">Printing Services</a></li>

			<span class="breadcrumb-divider"></span>


				<li>Photo Printing</li>




		</ul>

	</div>

	<div class="clearfix"></div>

		<div class="inner_content">

			<ul class="navigationTabs">

				<li class="upload col-md-3 col-sm-3 col-xs-3">

					<a class="active" href="#"><span class="text">1. Upload</span><span class="right active"></span></a>

				</li>

				<li id="sel2" class="paper col-md-3 col-sm-3 col-xs-3">

					<a class="inactive" href="#"><span class="left"></span><span class="text">2. Type</span><span class="right"></span></a>

				</li>

				<li id="sel3" class="paper col-md-3 col-sm-3 col-xs-3">

					<a class="inactive" href="#"><span class="left"></span><span class="text">3. Paper</span><span class="right"></span></a>

				</li>

				<li id="sel4" class="paper col-md-3 col-sm-3 col-xs-3">

					<a class="inactive" href="#"><span class="left"></span><span class="text">4. Order</span><span class="right"></span></a>

				</li>

				<span class="clearfix"></span>

			</ul>

			<div class="col-md-1">

			</div>

			<div class="col-md-5">

			<h1>Photo Printing</h1>

			<span class="delivery_span"><b>Free shipping for all orders above Rs. 50/-</b>

			<br>* Large volume orders may take longer. Read more about our <a href="/shipping/">Shipping Policy</a></span>



			<div style="display:none;" id="doc_viewer" class="doc_viewer"></div>

		 </div>



		<div class="col-md-5 preview_side">

			<div id="fk-alert"></div>

			<div id="step_1">

				<div class="step_1">

					<div id="fk-alert"></div>

					<div class="panel panel-primary">

						<div class="panel-heading">

							<h3 class="panel-title">Upload</h3>

						</div>

						<div class="panel-body">

							<div align="center" id="upload-wrapper">

								<div id="send-email">

									<input type="checkbox" class="css-checkbox" id="send_later">

									<label class="css-label" name="send_later" for="send_later">My photo is larger than 12MB. I will email it to sales@printview.in</label>

								</div>

								<br>

								<div id="upload-section">







									<span class="notice">Allowed Image Types: JPG &amp; PNG . | Maximum Size : 12 MB.</span>

									<form data-ajax="false" id="MyUploadForm" enctype="multipart/form-data" method="post" onsubmit="return false" action="<?php echo base_url('assets/processupload2.php'); ?>">

									<div id="file_up">

									<input type="file" id="imageInput" name="ImageFile">

									<input type="submit" value="Upload" id="submit-btn">

									<img alt="Please Wait" style="display:none;" id="loading-img" src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>">

									</div>

									<div style="display:none;" id="progressbox"><div id="progressbar"></div><div id="statustxt">0%</div></div>

									<div id="output"><input type="hidden" value="" id="file_loc" name="file_loc"></div>

									</form>

								</div>

								<div style="display:none;" id="file_pages"> </div>

								<div class="clearfix"></div>

								<p style="text-align:center; font-style:italic;"><strong>NOTE :</strong> Orders with invalid or incorrect data may lead to order cancellation. </p>

				<p style="text-align:center; font-style:italic;"><strong>Please Upload High Quality Image For Best Results. </strong> </p>

								<br>

								<a id="goto-2" class="btn btn-block btn-success">Next</a>

								<div class="clearfix"></div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="clearfix"></div>

				<div style="display:none;" id="step_2">

					<div class="step_2">

						<div class="panel panel-primary">

							<div class="panel-heading"><h3 class="panel-title">Type</h3></div>

							 <div class="panel-body text-center">

								  <div>



														   <input type="radio" checked="" style="display: none" value="Passport" id="option1" name="options">

						  <label for="option1">

							<div class="color select">

								<i>Passport</i>

								<img width="100%" height="100%" src="<?php echo base_url('assets/images/print-color.jpg'); ?>">

								<i>Rs. 1.00 /page</i>

							</div>

						  </label>

						  <input type="radio" style="display: none" value="4X6" id="option2" name="options">

						  <label for="option2">

							<div class="color select">

								<i>4X6</i>

								<img width="100%" height="100%" src="<?php echo base_url('assets/images/print-color.jpg'); ?>">

								<i>Rs. 6 /page</i>

							</div>

						  </label>

						  <input type="radio" style="display: none" value="A4" id="option3" name="options">

						  <label for="option3">

							<div class="color select">

								<i>A4</i>

								<img width="100%" height="100%" src="<?php echo base_url('assets/images/print-color.jpg'); ?>">

								<i>Rs. 20.00 /page</i>

							</div>

						  </label>





						  </div><br>

						  <a id="goto-3" class="btn btn-block btn-success">Next</a>



					 </div>





				  </div>

				</div>

				</div>

				<div class="clearfix"></div>

				<div style="display:none;" id="step_3" class="">

				<div class="step_3">

				<div class="panel panel-primary">

				   <div class="panel-heading">

					  <h3 class="panel-title">Paper Type</h3>

				   </div>

				   <div class="panel-body text-center">

				   <div style="display:none;" id="paper_passport">



						<input type="radio" checked="" style="display: none" value="260 GSM (Premium Coated)_0" id="p_type_passport0" name="p_type_passport">

						<label for="p_type_passport0"><div class="color select">

						<i>260 GSM (Premium Coated)</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Included </i>

					  </div></label>



						<input type="radio" style="display: none" value="Glossy RC Coated 270 GSM_1" id="p_type_passport1" name="p_type_passport">

						<label for="p_type_passport1"><div class="color select">

						<i>Glossy RC Coated 270 GSM</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Rs 1 /page</i>

					  </div></label>


				  </div>

				<div style="display:none;" id="paper_4x6">



					<input type="radio" style="display: none" checked="" value="260 GSM (Premium Coated)_2" id="p_type_4x60" name="p_type_4x6">

					  <label for="p_type_4x60"><div class="color select">

						<i>260 GSM (Premium Coated)</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Included </i>

					  </div></label>



					<input type="radio" style="display: none" value="270 GSM Premium RC (Water Proof)_4" id="p_type_4x61" name="p_type_4x6">

					  <label for="p_type_4x61"><div class="color select">

						<i>270 GSM Premium RC (Water Proof)</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Rs 4 /page</i>

					  </div></label>



					<input type="radio" style="display: none" value="5X7 Extended Size_4" id="p_type_4x62" name="p_type_4x6">

					  <label for="p_type_4x62"><div class="color select">

						<i>5X7 Extended Size</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Rs 4 /page</i>

					  </div></label>


				  </div>

				  <div style="display:none;" id="paper_a4">



					<input type="radio" style="display: none" checked="" value="180 GSM_0" id="p_type_a40" name="p_type_a4">

					  <label for="p_type_a40"> <div class="color select">

						<i>180 GSM</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Included </i>

					  </div></label>



					<input type="radio" style="display: none" value="220 GSM_4" id="p_type_a41" name="p_type_a4">

					  <label for="p_type_a41"> <div class="color select">

						<i>220 GSM</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Rs 4 /page</i>

					  </div></label>



					<input type="radio" style="display: none" value="240 GSM_8" id="p_type_a42" name="p_type_a4">

					  <label for="p_type_a42"> <div class="color select">

						<i>240 GSM</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Rs 8 /page</i>

					  </div></label>



					<input type="radio" style="display: none" value="Glossy RC Coated 270 GSM_12" id="p_type_a43" name="p_type_a4">

					  <label for="p_type_a43"> <div class="color select">

						<i>Glossy RC Coated 270 GSM</i>

						<img width="100%" height="100%" src="<?php echo base_url('assets/images/paper.jpg'); ?>">

						<i>Rs 12 /page</i>

					  </div></label>


				  </div>

				<br>

				  <a id="goto-4" class="btn btn-block btn-success">Next</a>

				   </div>

				</div>

			</div>

		 </div>

		<div class="clearfix"></div>

			<div style="display:none;" id="step_4" class="">

				  <div class="step_4">

					<div class="panel panel-primary">

					   <div class="panel-heading">

						  <h3 class="panel-title">No. of Copies</h3>

					   </div>

					   <div class="panel-body">

						  <div class="col-md-6">

							<div class="item_count">

								<div class="form-group">

									<label for="">Number of Copies :</label>

									<input type="text" value="1" placeholder="Number of Copies" id="c_count" name="c_count" class="form-control">

								 </div>



							  </div>

						 </div>

						 <div style="padding-top: 20px;" class="col-md-6">

							<span class="cross_X">X</span>

								 <span id="count_items" class="count_items">-</span>

						 </div>

						 <div class="clearfix"></div>

						 <div class="price_box">

								<span>Total Cost</span>

								<h4 id="total_price_h4">-</h4>

							  </div>

						 <div class="clearfix"></div>

						<a id="goto-6" class="btn btn-block btn-success">Calculate Total Charges</a><br>



						<a style="display:none;" id="step_6" class="btn btn-block btn-info">Confirm and Place Order</a>

					   </div>

					</div>

				  </div>

				  </div>

				  <div class="clearfix"></div>



				<div>

				  <h3 style="color: #000;text-align: center;font-style: italic;">Order Overview :</h3>

				  <table summary="Project Printing Order Overview" id="newspaper-a">



				  <tbody>

				  <tr>

				  <td>Item</td>

				  <td>Quality</td>

				  <td>Price</td>

				  <td>Sub-Total</td>

				  </tr>



				  <tr>

				  <td>Type</td>

				  <td>Photo Printing</td>

				  <td>-</td>

				  <td id="page_total">-</td>

				  </tr>



				  <tr>

				  <td>Printing</td>

				  <td id="print_type">-</td>

				  <td id="print_type_price">-</td>

				  <td id="print_type_tprice">-</td>

				  </tr>



				  <tr>

				  <td>Paper Quality</td>

				  <td id="paper_type">-</td>

				  <td id="paper_type_price">-</td>

				  <td id="paper_type_tprice">-</td>

				  </tr>







				  <tr>

				  <td>Copies</td>

				  <td>-</td>

				  <td id="copies_number">-</td>

				  <td id="copies_price">-</td>

				  </tr>



				  <tr>

				  <td style="background-color:#A4D483;">-</td>

				  <td style="background-color:#A4D483;">-</td>

				  <td style="background-color:#A4D483;"><b>Total : </b></td>

				  <td id="total_price" style="background-color:#A4D483;">-</td>

				  </tr>



				  </tbody>

				  </table>

				  </div>

				  <div class="clearfix"></div>

				<div>

					</div>

			</div>

			<div class="col-md-1">

			</div>

			<div class="clearfix"></div>



<div class="col-md-7 descript">

<h3>Photo Printing</h3>

<p>Online photo printing service at affordable rates with various printing sizes.&nbsp; Make your photos enhanced by professionals for&nbsp; more lively with vivid colours with PrintView .&nbsp; We use high quality papers to print your photos. Create a Collage Canvas of your favorite photos with our service. We offer bold , beautiful prints any size.</p>
<p><span style="color: #ff0000;">“Please Upload High Quality Images for best results”</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</div>

<div class="col-md-5 image_cont text-center"><br>

<img width="200" height="225" alt="Photo Printing Services India" class="attachment-post-thumbnail wp-post-image" data-lazy-src="http://www.printview.in/wp-content/uploads/2014/07/photo-printing.jpg" src="http://www.printview.in/wp-content/plugins/lazy-load/images/1x1.trans.gif"><noscript>&lt;img width="200" height="225" src="http://www.printview.in/wp-content/uploads/2014/07/photo-printing.jpg" class="attachment-post-thumbnail wp-post-image" alt="Photo Printing Services India" /&gt;</noscript>
</div>


			<div class="clearfix"></div>

		</div>

	  </div>

	 </div>

 <div class="push"></div>
