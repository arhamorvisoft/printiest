<div class="content">
<div class="container">
<br>
<br>
<div class="inner_content print_tables">
<div class="col-md-9 page_content">
<h1> Shipping Policy </h1>
<p> </p>
<ul>
<li>
<strong>
Standard Delivery
<span id="2d33cc50-d882-41d2-bee8-d949c1c8028a" class="GINGER_SOFTWARE_mark">
<span id="3584ae03-ab78-44cf-8110-2208d1f629a7" class="GINGER_SOFTWARE_mark">
<span id="002c2a66-bf05-4d23-91bd-985b15f48ba0" class="GINGER_SOFTWARE_mark"> :</span>
</span>
</span>
Available for All
<span id="a5695092-3c93-4eaa-8b69-20cc65d49b68" class="GINGER_SOFTWARE_mark">
<span id="0c443a04-6e1b-439d-bc62-bf4448b2cc75" class="GINGER_SOFTWARE_mark">
<span id="83fddbaf-c145-4917-953e-835aac52570a" class="GINGER_SOFTWARE_mark">orders</span>
</span>
</span>
(Only
<span id="0adf6c6a-b962-4791-814a-ce9d9d534b36" class="GINGER_SOFTWARE_mark">
<span id="179def99-50e2-4540-b78d-06752d487c7d" class="GINGER_SOFTWARE_mark">
<span id="7b2203e7-1dba-4fc1-8d58-270d99c2c6ed" class="GINGER_SOFTWARE_mark">Pre-Paid</span>
</span>
</span>
)
</strong>
</li>
<li>
<strong>
Cash on Delivery
<span id="bd4c53fd-4109-456c-81c1-c84b89058dbc" class="GINGER_SOFTWARE_mark">
<span id="a1d31df1-023c-4a11-9ad5-406deca030e9" class="GINGER_SOFTWARE_mark">
<span id="0dcb06ee-9ffb-4476-81aa-7510f015592f" class="GINGER_SOFTWARE_mark"> :</span>
</span>
</span>
Available in select regions
</strong>
</li>
<li>
<strong>
8 Hour Guaranteed Shipping
<span id="2edeae20-f018-4a78-b9ad-13e096c30367" class="GINGER_SOFTWARE_mark">
<span id="45f5877e-d4a8-4478-868b-78680ca72ae7" class="GINGER_SOFTWARE_mark">
<span id="a0ea2048-4b60-4549-9a08-ae5f9aedc77f" class="GINGER_SOFTWARE_mark"> :</span>
</span>
</span>
Available in select regions
</strong>
</li>
<li>
<strong>
Free Shipping
<span id="f050fc06-9eaf-4e4a-b332-7aa7096b7baf" class="GINGER_SOFTWARE_mark">
<span id="c38a709b-4c3b-482c-b1d3-4199a499ec2f" class="GINGER_SOFTWARE_mark">
<span id="2241c195-505a-46bc-b285-bebacee89798" class="GINGER_SOFTWARE_mark"> :</span>
</span>
</span>
For all orders above Rs. 30/-
</strong>
</li>
</ul>
<p>
<strong>We ship Orders all over India. Don�t worry about the delivery time. It will be shipped within 6 working hours after placing the order & our advanced tracking system will help you track it in real time.</strong>
</p>
<p>
<strong>If your area falls below 8 hours guaranteed delivery it will be delivered within 8 working hours.</strong>
</p>
<p>
<strong>
Working Hour:
<span style="color: #ff0000;">10:00 AM � 06:00 PM</span>
</strong>
</p>
<p>
<strong>
Please note that 8 hour delivery time will be calculated as
<span id="38f3305e-1705-425f-8c58-7c3ecd865b02" class="GINGER_SOFTWARE_mark">
<span id="8092d2e0-8c66-4b06-b5a3-f1eee5e7007c" class="GINGER_SOFTWARE_mark">
<span id="bac6be9a-b534-4386-b6cb-556153d95b6a" class="GINGER_SOFTWARE_mark"> :</span>
</span>
</span>
</strong>
</p>
<p>
<strong>
If order is placed in working hour 8 hours will be calculated from the Time of the order. If order is placed in non working hour it will be calculated from the next day�s working
<span id="28ee27cf-0024-4152-bfab-3b94b1f4b030" class="GINGER_SOFTWARE_mark">
<span id="f5dcdc37-1ab9-4a7b-b182-98c11bf2aa1c" class="GINGER_SOFTWARE_mark">
<span id="55b937d9-492c-4ff9-9a2e-4dec3f3e85f9" class="GINGER_SOFTWARE_mark">hour</span>
</span>
</span>
.
</strong>
</p>
<p>
<strong>
For further clarifications, please write to us at
<span style="color: #333399;">support@printview.in</span>
<a href="#">
<br>
C
</a>
all us:
<span class="skype_c2c_print_container notranslate">+91-9040190051</span>
<span id="skype_c2c_container" class="skype_c2c_container notranslate" data-ismobile="false" data-isrtl="false" data-isfreecall="false" data-numbertype="paid" data-numbertocall="+919040190051" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" tabindex="-1" dir="ltr">
<span class="skype_c2c_highlighting_inactive_common" skypeaction="skype_dropdown" dir="ltr">
<span id="non_free_num_ui" class="skype_c2c_textarea_span">
<img class="skype_c2c_logo_img" width="0" height="0" src="resource://skype_ff_extension-at-jetpack/skype_ff_extension/data/call_skype_logo.png">
<span class="skype_c2c_text_span">+91-9040190051</span>
<span class="skype_c2c_free_text_span"></span>
</span>
</span>
</span>
</strong>
</p>
</div>
<div class="col-md-3">
<div class="panel panel-info sidebox">
<div class="panel-body">
<span class="side_cont">
<strong style="padding-right:18px">Phone </strong>
:     
<span class="skype_c2c_print_container notranslate">+91-8213954606</span>
<span id="skype_c2c_container" class="skype_c2c_container notranslate" data-ismobile="false" data-isrtl="false" data-isfreecall="false" data-numbertype="paid" data-numbertocall="+918213954606" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" tabindex="-1" dir="ltr">
<span class="skype_c2c_highlighting_inactive_common" skypeaction="skype_dropdown" dir="ltr">
<span id="non_free_num_ui" class="skype_c2c_textarea_span">
<img class="skype_c2c_logo_img" width="0" height="0" src="resource://skype_ff_extension-at-jetpack/skype_ff_extension/data/call_skype_logo.png">
<span class="skype_c2c_text_span">+91-8213954606</span>
<span class="skype_c2c_free_text_span"></span>
</span>
</span>
</span>
</span>
<span class="side_cont">
<strong style="padding-right:24px">Email </strong>
:     sales@printview.in
</span>
<br>
<span class="side_cont">
<strong>Address  </strong>
:   PrintView Services Pvt. Ltd. , Plot No 1182/3294, Jaganmohan Nagar, Khandagiri, Bhubaneswar.
</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="push"></div>