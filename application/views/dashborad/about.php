<div class="content">
<div class="container">
<br>
<br>
<div class="inner_content print_tables">
<div class="col-md-9 page_content">
<h1> About </h1>
<h2></h2>
<p>Hello!</p>
<p>
It’s nice of you to show interest in us. Here are few things you might like to know.
<br>
Printview started its operation in 2013 with the objective of making affordable printing in Bhubaneswar. Then we realised we need a platform where we can take order easily from anywhere and can provide better service. Although we have very few service at this point of time but we believe that we have delivered a good service quality to our customers.
</p>
<p>Our service includes Document Printing, Letterhead Printing, Envelop Printing, Self-Customized business card and corporate printing solutions.</p>
<p> </p>
<p>If you have any queries/Need any information mail us support@printview.in.</p>
<p> </p>
</div>
<div class="col-md-3">
<div class="panel panel-info sidebox">
<div class="panel-body">
<span class="side_cont">
<strong style="padding-right:18px">Phone </strong>
:
<span class="skype_c2c_print_container notranslate">+91-8213954606</span>
<span id="skype_c2c_container" class="skype_c2c_container notranslate" data-ismobile="false" data-isrtl="false" data-isfreecall="false" data-numbertype="paid" data-numbertocall="+918213954606" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" tabindex="-1" dir="ltr">
<span class="skype_c2c_highlighting_inactive_common" skypeaction="skype_dropdown" dir="ltr">
<span id="non_free_num_ui" class="skype_c2c_textarea_span">
<img class="skype_c2c_logo_img" width="0" height="0" src="resource://skype_ff_extension-at-jetpack/skype_ff_extension/data/call_skype_logo.png">
<span class="skype_c2c_text_span">+91-8213954606</span>
<span class="skype_c2c_free_text_span"></span>
</span>
</span>
</span>
</span>
<span class="side_cont">
<strong style="padding-right:24px">Email </strong>
:     sales@printview.in
</span>
<br>
<span class="side_cont">
<strong>Address  </strong>
:   PrintView Services Pvt. Ltd. , Plot No 1182/3294, Jaganmohan Nagar, Khandagiri, Bhubaneswar.
</span>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="push"></div>
