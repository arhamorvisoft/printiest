<head>

      <meta charset="utf-8">

      <title><?php echo $pageTitle; ?></title>

      <meta name="robots" content="index, follow">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" type="text/css" rel="stylesheet" >
      <link href="<?php echo base_url('assets/css/jquery.countdownTimer.css'); ?>" type="text/css" rel="stylesheet">
      <link href="<?php echo base_url('assets/css/style.css'); ?>" type="text/css" rel="stylesheet" >


      <!-- All in One SEO Pack 2.2.7.2 by Michael Torbert of Semper Fi Web Design[58,110] -->
      <meta name="description" itemprop="description" content="Print View provides affordable online document printing, project printing, letter head printing, photo printing, envelope printing and visiting card printing" />

      <meta name="keywords" itemprop="keywords" content="online, printing, delivery, project, document, letter head, visiting card, envelope, photo, india, delivery, free" />

      <!-- /all in one seo pack -->
      <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.printview.in\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.3"}};
            !function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
            img.wp-smiley,
            img.emoji {
                  display: inline !important;
                  border: none !important;
                  box-shadow: none !important;
                  height: 1em !important;
                  width: 1em !important;
                  margin: 0 .07em !important;
                  vertical-align: -0.1em !important;
                  background: none !important;
                  padding: 0 !important;
            }
      </style>
      <link rel='stylesheet' id='wpmu-animate-min-css-css'  href='<?php echo base_url('assets/css/animate.min.css'); ?>?ver=4.3.3' type='text/css' media='all' />
      <script type='text/javascript' src='<?php echo base_url('assets/js/jquery.js'); ?>?ver=1.11.3'></script>
      <script type='text/javascript' src='<?php echo base_url('assets/js/jquery-migrate.min.js'); ?>?ver=1.2.1'></script>

      <meta name="generator" content="WordPress 4.3.3" />
      <!-- Woopra code starts here -->
      <script>
            (function(){
                  var t,i,e,n=window,o=document,a=arguments,s="script",r=["config","track","identify","visit","push","call"],c=function(){var t,i=this;for(i._e=[],t=0;r.length>t;t++)(function(t){i[t]=function(){return i._e.push([t].concat(Array.prototype.slice.call(arguments,0))),i}})(r[t])};for(n._w=n._w||{},t=0;a.length>t;t++)n._w[a[t]]=n[a[t]]=n[a[t]]||new c;i=o.createElement(s),i.async=1,i.src="//static.woopra.com/js/w.js",e=o.getElementsByTagName(s)[0],e.parentNode.insertBefore(i,e)
            })("woopra");
            woopra.config({"app":"wordpress","domain":"printview.in","download_tracking":false,"outgoing_tracking":false,"hide_campaign":false});
            woopra.track();
      </script>
      <!-- Woopra code ends here -->
      <!-- Start of Woopra Code -->
      <script>
            (function(){
                  var t,i,e,n=window,o=document,a=arguments,s="script",r=["config","track","identify","visit","push","call","trackForm","trackClick"],c=function(){var t,i=this;for(i._e=[],t=0;r.length>t;t++)(function(t){i[t]=function(){return i._e.push([t].concat(Array.prototype.slice.call(arguments,0))),i}})(r[t])};for(n._w=n._w||{},t=0;a.length>t;t++)n._w[a[t]]=n[a[t]]=n[a[t]]||new c;i=o.createElement(s),i.async=1,i.src="//static.woopra.com/js/w.js",e=o.getElementsByTagName(s)[0],e.parentNode.insertBefore(i,e)
            })("woopra");

            woopra.config({
                  domain: "printview.in"
            });
            woopra.track();
      </script>
</head>
