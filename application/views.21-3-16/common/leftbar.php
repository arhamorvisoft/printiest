<div id="left-menu-toggle">
      <img src="http://www.printview.in/wp-content/themes/PrintView/images/left-menu.png" >
</div>
<div id="left-menu">
      <div class="col-md-offset-1 col-md-10">
            <h2 class="text-center" style="white-space:nowrap;">Feedback</h2>
            <div class="login_loading" id="contact_loading" style="display:none;">
                  <img src="http://www.printview.in/wp-content/themes/PrintView/images/loading.gif" alt="Loading ...">
            </div>
            <div class="login_done" id="contact_done" style="display:none;"></div>

            <form class="form-horizontal" role="form" name="feed_form" id="feed_form" method="post" action="/contact/">
                  <div class="form-group">
                        <input type="text" class="form-control" id="feed_name" name="feed_name" placeholder="Enter your Name" required>
                  </div>
                  <div class="form-group">
                        <input type="text" class="form-control" id="feed_email" name="feed_email" placeholder="Enter your Email" required>
                  </div>
                  <div class="form-group">
                        <input type="text" class="form-control" id="feed_mobile" name="feed_mobile" placeholder="Enter your Mobile Number" required>

                  </div>
                  <div class="form-group">
                        <textarea class="form-control" placeholder="Enter your Message here" rows="6" style="resize:none" name="feed_message" id="feed_message" required></textarea>
                  </div>
                  <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                  </div>
            </form>
      </div>

</div>
