<footer class="footer">
           <div class="container">
            <div class="col-md-3">
                <h3>Online Printing Services</h3>
                <ul>
                  <li> <a href="/document-printing/">Document Printing</a> </li>
                  <li> <a href="/project-printing/">Project Printing</a> </li>
                  <li> <a href="/photo-printing/">Photo Printing</a> </li>
                <li> <a href="/letter-head/">LetterHead Printing</a> </li>
                  <li> <a href="/envelope/">Envelope Printing</a> </li>
                  <li> <a href="/visiting-card/">Visiting Card Printing</a> </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h3>Orders &amp; Shipping</h3>
                      <ul>
                        <li> <a href="/shipping/">Shipping Policy</a> </li>
                        <li> <a href="/track/">Track your Order</a> </li>
                        <li> <a href="/products/">All Print Products</a> </li>
                      <li> <a href="/refund-policy/">Refund Policy</a> </li>
                    <li class="social_icos "><a href="https://www.facebook.com/printview.in" target="_blank" class="FB"></a><a href="https://plus.google.com/114269517479173784152" rel="publisher" target="_blank" class="GP"></a><a href="https://twitter.com/printview_in" target="_blank" class="TW"></a> </li>
                      </ul>
     </div>
         <div class="col-md-3">
                <h3>Accounts</h3>

                  <ul>

            <li> <a href="/login/">Login to your Account</a> </li>
                  <li> <a href="/register">Register for a new Account</a> </li>

                <li> <a href="/password-recovery/">Password Recovery</a></li>
                    <li> <a href="/about/">About Us</a></li>
                   <li> <a href="/contact/">Contact Us</a></li>

                  </ul>

            </div>

            <div class="col-md-3">

                  <ul>
               <li style="text-align:center;"> <img src="" width="150"> </li>
               <li style="text-align:center;"> <img src="<?php echo base_url('assets/images/lowest-price-guarantee.png'); ?>" width="200"> </li>
                </ul>
           </div>
       </div>
 <div class="bottom_foot">
  <div class="container">
         <span class="col-md-6"><a href="/privacy-policy/">Privacy Policy</a> | <a href="/terms-of-use/">Terms of Use</a> | <a href="http://blog.printview.in/"target="_blank">Blog</a></span>
          <span class="col-md-6 copy_rights">Copyright © 2015 Printiest.pk | All rights reserved.</span>
     </div>

   </div>
     </footer>

<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.cookie.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.form.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {

  $('#left-menu-toggle').click(function(){
    if($('#left-menu').hasClass('left-open')){
      $('#left-menu').removeClass('left-open');
      $('#left-menu-toggle').removeClass('left-open');

    }else{
      $('#left-menu').addClass('left-open');
      $('#left-menu-toggle').addClass('left-open');

    }

    $("#wrapper").click(function(e) {
      $('#left-menu').removeClass('left-open');
      $('#left-menu-toggle').removeClass('left-open');

    });

});

  $('#right-menu-toggle').click(function(){
    if($('#right-menu').hasClass('right-open')){
      $('#right-menu').removeClass('right-open');

      $('#right-menu-toggle').removeClass('right-open');

    }else{

      $('#right-menu').addClass('right-open');
      $('#right-menu-toggle').addClass('right-open');
    }
    $("#wrapper").click(function(e) {
      $('#right-menu').removeClass('right-open');

    $('#right-menu-toggle').removeClass('right-open');
    });
  });

});

</script>

<script>
$(document).ready(function(){

  $.validator.addMethod("alpha", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
  });

  $("#feed_form").validate({
    rules: {
      feed_name: {required: true,alpha:true},
      feed_email: {required: true,email:true},

      feed_mobile: {required: true,number: true,minlength: 10,maxlength:10},
      feed_message: {required: true},
  },

    messages: {

      feed_name: {

        required: "Please enter your Name.",

        alpha:"Name should contains alphabets and space only."

            },

      feed_email:{

        required: "Please enter your Email.",
        email:"Please enter a valid Email (Eg: someone@example.com)."

      },

      feed_mobile: {

        required:"Please enter 10 digit Mobile number.",

        number: "Please enter 10 digit Mobile number.",

        minlength: "Please enter 10 digit Mobile number.",

        maxlength: "Please enter 10 digit Mobile number."
      },
      feed_message: {
        required: "Please enter your Message."
      }
    },
        submitHandler: function(form) {

      var feed_name = jQuery("#feed_name").val();
     var feed_email = jQuery("#feed_email").val();
      var feed_mobile = jQuery("#feed_mobile").val();

      var feed_message = jQuery("#feed_message").val();


      jQuery.ajax({

              url: '/wp-admin/admin-ajax.php',

                type : "POST",

        data:{ action: 'feedback', feed_name: feed_name,feed_email: feed_email,feed_mobile: feed_mobile,feed_message: feed_message},

        beforeSend : function() { $("#contact_done").hide('');$('#contact_loading').show(); },

        success: function(data, textStatus, XMLHttpRequest)

        {

          switch (data)

         {

            case '0':

              jQuery("#contact_done").html('');

                var  lstat="<p style='color:#FF7474;'>Please check your inputs.</p>";
                jQuery("#contact_done").append(lstat);

            break;

            case '1':

              jQuery("#contact_done").html('');
                var lstat="<p style='color:#21610B;'>Your message hasb een successfully sent</p>";
                jQuery("#contact_done").append(lstat);
            break;

            default:
              jQuery("#contact_done").html('');
                var  lstat="<p style='color:#FF7474;'>Some error occured. Please try after some time.</p>";

                jQuery("#contact_done").append(lstat);
            break;
          }
          $('#contact_loading').hide();
          $('#contact_done').show();
        }
      });
      return false;
    },
  });
});
</script>

<script>
$(document).ready(function(){

  $.validator.addMethod("alpha", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);

  });

  $("#ask_form").validate({

    rules: {
      ask_name: {required: true,alpha:true},

      ask_email: {required: true,email:true},

      ask_mobile: {required: true,number: true,minlength: 10,maxlength:10},
      ask_message: {required: true},
    },

    messages: {
      ask_name: {

        required: "Please enter your Name.",

        alpha:"Name should contains alphabets and space only."

          },

      ask_email:{

        required: "Please enter your Email.",

        email:"Please enter a valid Email (Eg: someone@example.com)."

      },

      ask_mobile: {

        required:"Please enter 10 digit Mobile number.",

        number: "Please enter 10 digit Mobile number.",
        minlength: "Please enter 10 digit Mobile number.",

        maxlength: "Please enter 10 digit Mobile number."
      },

      ask_message: {
        required: "Please enter your Message."
      }
    },
        submitHandler: function(form) {
      var ask_name = jQuery("#ask_name").val();
      var ask_email = jQuery("#ask_email").val();
      var ask_mobile = jQuery("#ask_mobile").val();
      var ask_message = jQuery("#ask_message").val();

      jQuery.ajax({

             url: '/wp-admin/admin-ajax.php',
              type : "POST",
      data:{ action: 'ask_designer', ask_name: ask_name,ask_email: ask_email,ask_mobile: ask_mobile,ask_message: ask_message},
        beforeSend : function() { $("#contact_done2").hide('');$('#contact_loading2').show(); },
        success: function(data, textStatus, XMLHttpRequest)
        {

          switch (data)
          {
            case '0':

              jQuery("#contact_done2").html('');
                var  lstat="<p style='color:#FF3300;'>Please check your inputs.</p>";
                jQuery("#contact_done2").append(lstat);

          break;

          case '1':

              jQuery("#contact_done2").html('');
                var lstat="<p style='color:#FF0000;'>Your message has been successfully sent</p>";
                jQuery("#contact_done2").append(lstat);

          break;

        default:



              jQuery("#contact_done2").html('');



                var  lstat="<p style='color:#FF3300;'>Some error occured. Please try after some time.</p>";



                jQuery("#contact_done2").append(lstat);
            break;

          }

          $('#contact_loading2').hide();

          $('#contact_done2').show();
        }

      });

      return false;
    },
  });
});
</script>

<script type="text/javascript">

$(document).ready(function() {




  var upload_status='Pending';

  var progressbox     = $('#progressbox');

  var countdowntimer     = $('#countdowntimer');

  var progressbar     = $('#progressbar');

  var statustxt       = $('#statustxt');

  var completed       = '0%';



  var options = {

      target:   '#output',   // target element(s) to be updated with server response

      beforeSubmit:  beforeSubmit,  // pre-submit callback

      uploadProgress: OnProgress,

      success:       afterSuccess,  // post-submit callback

      resetForm: true        // reset the form after successful submit

    };


   $('#MyUploadForm').submit(function() {

//$("#countdowntimer").show();
$("#ms_timer").show();


        $("#progressTimer").progressTimer({
          timeLimit: $("#restTime").val()
        });


      $(this).ajaxSubmit(options);






      // return false to prevent standard browser submit and page navigation

      return false;

    });



//when upload progresses

function OnProgress(event, position, total, percentComplete)

{


 countdowntimer.show();


  //Progress bar

  progressbar.width(percentComplete + '%') //update progressbar percent complete

  statustxt.html(percentComplete + '%'); //update status text

  if(percentComplete>50)

    {

      statustxt.css('color','#fff'); //change status text to white after 50%

    }

}



//after succesful upload

function afterSuccess()

{

  $('#submit-btn').show(); //hide submit button

  $('#loading-img').hide(); //hide submit button
$("#countdowntimer").hide();
$("#ms_timer").hide();

  var doc_file = $('#file_loc').val();

  $("#doc_viewer").html("<iframe id='iframeId' src='http://docs.google.com/viewer?url=http%3A%2F%2Fwww.printview.in%2Fpdf%2F"+ doc_file +"&embedded=true' width='100%' height='580'></iframe>");

    var iframe = document.getElementById('iframeId');

    var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;

    var ulObj = innerDoc.getElementById("controlbarPageNumber");

    upload_status = "Success";

  $("#doc_viewer2").html(ulObj);

  $("#doc_viewer").show();

   $('#file_up').hide(); //hide file upload option



}



//function to check file size before uploading.

function beforeSubmit(){

    //check whether browser fully supports all File API

   if (window.File && window.FileReader && window.FileList && window.Blob)

  {



    if( !$('#imageInput').val()) //check empty input filed

    {

      $("#output").html("<b  style='color:#FF7474;'>Please select a document to upload.</b>");

      return false

    }



    var fsize = $('#imageInput')[0].files[0].size; //get file size

    var ftype = $('#imageInput')[0].files[0].type; // get file type

  var name = $('#imageInput')[0].files[0].name;

  var file_name= name;

  $.removeCookie("file_name");

  $.cookie("file_name", file_name,{ path: '/' });



    //allow only valid document file types

    switch(ftype)

        {

     case 'application/pdf':

                break;

            default:

                $("#output").html("<b  style='color:#FF7474;'>Unsupported file type!</b>");

        return false

        }



    //Allowed file size is less than 12 MB (12582912)

    if(fsize>12582912)

    {

      $("#output").html("<b>"+bytesToSize(fsize) +"</b> File is larger than 12MB <br />Please reduce the size of your document or email it to us.");

      return false

    }



    //Progress bar

    progressbox.show(); //show progressbar
  countdowntimer.show(); //show progressbar

    progressbar.width(completed); //initial value 0% of progressbar

    statustxt.html(completed); //set status text

    statustxt.css('color','#000'); //initial color of status text





    $('#submit-btn').hide(); //hide submit button

    $('#loading-img').show(); //hide submit button

    $("#output").html("");

    $("#send-email").hide("slow").fadeOut("slow"); //hide send via email option



  }

  else

  {

    //Output error to older unsupported browsers that doesn't support HTML5 File API

    $("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");

    return false;

  }

}

//function to format bites bit.ly/19yoIPO

function bytesToSize(bytes) {

   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

   if (bytes == 0) return '0 Bytes';

   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];

}



  var b_w_price='1.50';

  var color_price='4.00';



// file upload or by email

var bla = $('#file_loc').val();

$('#send_later').change(function() {

  if($(this).is(":checked")) {

  $("#upload-section").hide("slow").fadeOut("slow");

  $('#file_loc').val("Document will be emailed to sales@printview.in");

  $( "#file_pages" ).html( "<div class='form-group'><div class='col-md-6'><label for='page_count'>Number of Pages in document :</label></div><div class='col-md-6'><input class='form-control' name='page_count' id='page_count' placeholder='Number of Pages in document'  type='text'></div><div class='clearfix'></div></div>" );

  return;

  }

   $("#upload-section").show("slow").fadeIn("slow");

   $( "#file_pages" ).html( "" );

   $('#file_loc').val(bla);

});





// file upload or by email

$( "#goto-2" ).click(function() {

  $( "#fk-alert" ).html( "" );

  var fileName = $("#imageInput").val();

  var s2_p_count = $('#page_count').val();

    $.removeCookie("s2_p_count",{ path: '/' });

  $.cookie("s2_p_count", s2_p_count,{ path: '/' });

  var ch_value = $('#file_loc').val();

    $.removeCookie("file_name",{ path: '/' });

  $.cookie("file_name", ch_value,{ path: '/' });

  if ( upload_status =='Pending' &&  $('#send_later').is(":unchecked"))

  { $( "#fk-alert" ).html( "<div class='fk-alert'>Please upload your document to continue.</div>" );  }

  else if ( ((Math.floor($.cookie("s2_p_count")) != $.cookie("s2_p_count"))) || ($.cookie("s2_p_count") < 1 ) )

  { $( "#fk-alert" ).html( "<div class='fk-alert'>Please provide the number of pages in the document.</div>" ); }

  else

  {

  $( "#fk-alert" ).html( "" );

  $("#step_1").hide("slow").fadeOut("slow");

  $("#step_2").show("slow").fadeIn("slow");

  $( "#sel2" ).html( "" );

  $( "#sel2" ).html( "<a href='#' class='active'><span class='left'></span><span class='text'>2. Paper</span><span class='right'></span></a>" );

  $( "#page_total" ).html( $.cookie("s2_p_count") + " Page(s)");

    }







});







// Black & White  or Color

$( "#goto-3" ).click(function() {

  var s1_type = $('input:radio[name=options]:checked').val();

  $.removeCookie("s1_type");

  $.cookie("s1_type", s1_type ,{ path: '/' });

  $("#step_2").hide("slow").fadeOut("slow");

    $("#step_3").show("slow").fadeIn("slow");

    if ($.cookie("s1_type") == "BW" ) { $("#paper_bw").show(); }

    if ($.cookie("s1_type") == "COLOR" ) { $("#paper_color").show(); }

  if ($.cookie("s1_type") == "BW")

    {

  $( "#print_type" ).html( "Black & White" );

  $( "#print_type_price" ).html( "1.50 x " + $.cookie("s2_p_count") ); $('#print_type_tprice').html(($.cookie("s2_p_count") * 1.50 )).toFixed(2);

  }



  else if ($.cookie("s1_type") == "COLOR")

    {

  $( "#print_type" ).html( "Colour" );

  $( "#print_type_price" ).html( "4.00 x " + $.cookie("s2_p_count") ); $('#print_type_tprice').html(($.cookie("s2_p_count") * 4.00 )).toFixed(2);

  }

});



// Paper type

$( "#goto-4" ).click(function() {

  if ($.cookie("s1_type") == "BW" ) { var p_type = $('input:radio[name=p_type_bw]:checked').val(); }

  if ($.cookie("s1_type") == "COLOR" ) { var p_type = $('input:radio[name=p_type_color]:checked').val(); }

  var arr = p_type.split('_');

  var b_options = $('input:radio[name=b_options]:checked').val();

  $.removeCookie("s3_p_type");

  $.removeCookie("s3_binding");

  $.cookie("s3_p_type", p_type,{ path: '/' });

  $.cookie("s3_binding", b_options,{ path: '/' });

  $( "#paper_type" ).html( arr[0] );

  $( "#paper_type_price" ).html( arr[1] + " x " + $.cookie("s2_p_count"));

   $('#paper_type_tprice').html(($.cookie("s2_p_count") * arr[1]).toFixed(2) );



  $("#step_3").hide("slow").fadeOut("slow");

  $("#step_5").show("slow").fadeIn("slow");

  $( "#sel3" ).html( "" );

  $( "#sel3" ).html( "<a href='#' class='active'><span class='left'></span><span class='text'>3. Binding</span><span class='right'></span></a>" );

});



// Binding

$( "#goto-5" ).click(function() {

  var b_options = $('input:radio[name=b_options]:checked').val();

  $.removeCookie("s3_binding");

  $.cookie("s3_binding", b_options,{ path: '/' });



  if (($.cookie("s3_binding")=="spiral")) { $( "#binding_type" ).html("Spiral"); $( "#binding_type_price" ).html("25.00"); $( "#binding_type_tprice" ).html("25.00"); }

  else if (($.cookie("s3_binding")=="none")) { $( "#binding_type" ).html("None"); $( "#binding_type_price" ).html("0"); $( "#binding_type_tprice" ).html("0"); }



  $("#step_5").hide("slow").fadeOut("slow");

  $("#step_4").show("slow").fadeIn("slow");



});





// No of Copies

$( "#goto-6" ).click(function() {

  if( ($('#c_count').val()=="") || (Math.floor($('#c_count').val()) != $('#c_count').val()) || ($('#c_count').val() < 1 ) )

  { $( "#fk-alert" ).html( "<div class='fk-alert'>Please provide the number of copies required.</div>" ); }

  else

  {

  var t1=$('#print_type_tprice').html();

  var t2=$('#paper_type_tprice').html();

  var t3=$('#binding_type_tprice').html();

  var single_copy_total = parseFloat(t1) + parseFloat(t2) + parseFloat(t3);

  var s4_copies = $('#c_count').val();

  $.removeCookie("s4_copies");

  $.cookie("s4_copies", s4_copies,{ path: '/' });

  $( "#copies_number" ).html($.cookie("s4_copies") + " x " + single_copy_total);

  $( "#copies_price" ).html(($.cookie("s4_copies") * single_copy_total).toFixed(2));

  $( "#count_items" ).html("Rs. " + (single_copy_total).toFixed(2) + "/-");

  $( "#total_price" ).html(($.cookie("s4_copies") * single_copy_total).toFixed(2));

  $( "#total_price_h4" ).html("Rs. " + ($.cookie("s4_copies") * single_copy_total).toFixed(2) + "/-");

  $( "#fk-alert" ).html( "" );

  $("#step_6").show("slow").fadeIn("slow");

  $( "#sel4" ).html( "" );

  $( "#sel4" ).html( "<a href='#' class='active'><span class='left'></span><span class='text'>4. Order</span><span class='right'></span></a>" );

  }

});



$( "#step_6" ).click(function() {



  $.cookie("printing_type", 'Document Printing',{ path: '/' });

  var pr_type=$('#print_type').html();

  $.cookie("pr_type", pr_type,{ path: '/' });

  var sub_total=$('#page_total').html();

  $.cookie("page_total", sub_total,{ path: '/' });

  var pt_price=$('#print_type_price').html();

  $.cookie("pt_price", pt_price,{ path: '/' });

  var pt_s_price=$('#print_type_tprice').html();

  $.cookie("pt_s_price", pt_s_price,{ path: '/' });

  var paper_typ=$('#paper_type').html();

  $.cookie("paper_typ", paper_typ,{ path: '/' });

  var paper_typ_price=$('#paper_type_price').html();

  $.cookie("paper_typ_price", paper_typ_price,{ path: '/' });

  var paper_typ_s_price=$('#paper_type_tprice').html();

  $.cookie("paper_typ_s_price", paper_typ_s_price,{ path: '/' });

  var copies=$('#copies_number').html();

  $.cookie("copies", copies,{ path: '/' });

  var copies_total=$('#copies_price').html();

  $.cookie("copies_total", copies_total,{ path: '/' });

  var total=$('#total_price').html();

  $.cookie("total", total,{ path: '/' });

  window.location.assign("http://www.printview.in/cart/?add=yes")

});



});
</script>
