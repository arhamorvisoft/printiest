<div id="right-menu-toggle">
    <img src="http://www.printview.in/wp-content/themes/PrintView/images/right-menu.png" >
</div>

<div id="right-menu">
    <div class="col-md-offset-1 col-md-10">
        <h2 class="text-center">Ask a Designer</h2>
        <div class="login_loading" id="contact_loading2" style="display:none;">
            <img src="http://www.printview.in/wp-content/themes/PrintView/images/loading.gif" alt="Loading ...">

        </div>
        <div class="login_done" id="contact_done2" style="display:none;"></div>
        <form class="form-horizontal" role="form" name="ask_form" id="ask_form" method="post" action="/contact/">
            <div class="form-group">
                <input type="text" class="form-control" id="ask_name" name="ask_name" placeholder="Enter your Name" required>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="ask_email" name="ask_email" placeholder="Enter your Email" required>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="ask_mobile" name="ask_mobile" placeholder="Enter your Mobile Number" required>
            </div>
            <div class="form-group">
                <textarea class="form-control" placeholder="Enter your Message here" rows="6" style="resize:none" name="ask_message" id="ask_message" required></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">Submit</button>
            </div>
        </form>
    </div>
</div>
