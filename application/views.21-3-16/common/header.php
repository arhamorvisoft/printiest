<header class="header">
                  <div class="container">
                        <div class="col-md-3 logo">
                              <a href="/"> <img src="<?php echo base_url('assets/images/logo.png'); ?>" width="200px" ></a></div>
                        <div class="col-md-5"><div class="navmenu">
                              <ul>
                                    <li><a href="<?php echo site_url('Products/product'); ?>">Products</a>
                                          <ul>
                                                <li><a href="<?php echo site_url('Products/document_printing'); ?>">Document Printing</a></li>
                                                <li><a href="<?php echo site_url('Products/project_printing'); ?>">Project Printing</a></li>
                                                <li><a href="<?php echo site_url('Products/letter_head'); ?>">Letter Head</a></li>
                                                <li><a href="<?php echo site_url('Products/photo_printing'); ?>">Photo Printing</a></li>
                                                <li><a href="<?php echo site_url('Products/envelope'); ?>">Envelope</a></li>
                                                <li><a href="<?php echo site_url('Products/visiting_card'); ?>">Visiting Card</a></li>
                                          </ul>
                                    </li>
                                    <li><a href="<?php echo site_url('welcome/shipping'); ?>">Shipping Info</a></li>
                                    <li><a href="<?php echo site_url('welcome/about'); ?>">About Us</a>
                                          <ul>
                                                <li><a href="http://blog.printview.in">Blog</a></li>
                                          </ul>
                                   </li>
                              </ul>
                              </div></div>
                        <div class="user_log col-md-4">
                              <span class="top_link tl_login"><a href="<?php echo site_url('welcome/login'); ?>">Login</a></span>
                              <span class="top_link tl_reg"><a href="<?php echo site_url('welcome/register'); ?>">Register</a></span>
                        <span class="top_link tl_cart">
                                    <span class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="/cart/">My Cart [ 0 ]<span class="caret"></span></a>
                                          <ul class="dropdown-menu">
                                                <li><span class="cart_items">
                                                      <a href="/products/">Your cart is empty</a>
                                                      </span></li>
                                                <li style="border-top:1px solid;"><span class="cart_items">
                                                      <a href="/products/">Grand Total : </a><b>Rs. 0.00/-</b>
                                                      </span></li>
                                                <li class="cart_check">
                                                      <a class="btn btn-success" href="/products/">Add Items</a>
                                                </li>
                                          </ul>
                                    </span>
                              </span>
                        </div>
                        <div class="clearfix"></div>
                  </div>
            </header>
