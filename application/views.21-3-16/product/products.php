<div class="content">
    <div class="container">
        <div class="col-md-4 col-sm-4">
            <a href="<?php echo site_url('Products/document_printing'); ?>">
                <div class="block block_black">
                    <h2>DOCUMENT PRINTING</h2>
                    <h3>@ Rs. 1.50/- onwards</h3>
                    <div class="block_hover">
                        <p class="btn btn-primary btn-sm">Order Now!</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4">
            <a href="<?php echo site_url('Products/project_printing'); ?>">
                <div class="block block_pink">
                    <h2>PROJECT PRINTING</h2>
                    <h3>@ Rs. 1.50/- onwards</h3>
                    <div class="block_hover">
                        <p class="btn btn-primary btn-sm">Order Now!</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4">
            <a href="<?php echo site_url('Products/letter_head'); ?>">
                <div class="block block_yellow">
                    <h2>LETTER HEAD</h2>
                    <h3>@ Rs. 3.50/- onwards</h3>
                    <div class="block_hover">
                        <p class="btn btn-primary btn-sm">Order Now!</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="container">
        <div class="col-md-4 col-sm-4">
            <a href="<?php echo site_url('Products/photo_printing'); ?>">
                <div class="block block_green">
                    <h2>PHOTO PRINTING</h2>
                    <h3>@ Re. 1.00/- onwards</h3>
                    <div class="block_hover">
                        <p class="btn btn-primary btn-sm">Order Now!</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4">
            <a href="<?php echo site_url('Products/envelope'); ?>">
                <div class="block block_violet">
                    <h2>ENVELOPE PRINTING</h2>
                    <h3>@ Rs. 2.5/- onwards</h3>
                    <div class="block_hover">
                        <p class="btn btn-primary btn-sm">Order Now!</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4">
            <a href="<?php echo site_url('Products/visiting_card'); ?>">
                <div class="block block_brown">
                    <h2>VISITING CARD</h2>
                    <h3>@ Rs. 1/- onwards</h3>
                    <div class="block_hover">
                        <p class="btn btn-primary btn-sm">Order Now!</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="push"></div>
