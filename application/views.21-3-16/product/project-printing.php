<div class="content">
	<div class="container">
		<div class="proof-points">
			<ul>
				<li class="proof_click"><a href="#">Online Printing Services</a></li>
				<li class="proof_del"><a href="#">Cash on Delivery</a></li>
				<li class="proof_sameday"><a href="#">8 Hour Delivery</a></li>
			</ul>
		</div>
		<div class="breadcrumbs col-md-12">
			<ul class="col-md-9">
				<li><a title="Go to Home Page" href="/">Home</a></li>
				<span class="breadcrumb-divider"></span>
				<li><a href="/products/">Printing Services</a></li>
				<span class="breadcrumb-divider"></span>
				<li>Project Printing</li>

			</ul>
		</div>
		<div class="clearfix"></div>
		<div class="inner_content">
			<ul class="navigationTabs">
				<li class="upload col-md-3 col-sm-3 col-xs-3">
					<a class="active" href="#"><span class="text">1. Upload</span><span class="right active"></span></a>
				</li>
				<li id="sel2" class="paper col-md-3 col-sm-3 col-xs-3">
					<a class="inactive" href="#"><span class="left"></span><span class="text">2. Paper</span><span class="right"></span></a>
				</li>
				<li id="sel3" class="paper col-md-3 col-sm-3 col-xs-3">
					<a class="inactive" href="#"><span class="left"></span><span class="text">3. Binding</span><span class="right"></span></a>
				</li>
				<li id="sel4" class="paper col-md-3 col-sm-3 col-xs-3">
					<a class="inactive" href="#"><span class="left"></span><span class="text">4. Order</span><span class="right"></span></a>
				</li>
				<span class="clearfix"></span>
			</ul>
			<div class="col-md-1">
			</div>
			<div class="col-md-5">
				<h1>Project Printing</h1>
				<span class="delivery_span"><b>Free shipping for all orders above Rs. 50/-</b>
					<br>* Large volume orders may take longer. Read more about our <a href="/shipping/">Shipping Policy</a></span>
				<div id="doc_viewer2" class="doc_viewer2"></div>
				<div style="display:none;" id="doc_viewer" class="doc_viewer"></div>
			</div>

			<div class="col-md-5 preview_side">
				<div id="fk-alert"></div>
				<div id="step_1">
					<div class="step_1">
						<div id="fk-alert"></div>
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Upload</h3>
							</div>
							<div class="panel-body">
								<div align="center" id="upload-wrapper">
									<div id="send-email">
										<input type="checkbox" class="css-checkbox" id="send_later">
										<label class="css-label" name="send_later" for="send_later">My file is larger than 12MB. I will email it to sales@printview.in</label>
									</div>
									<br>
									<div id="upload-section">



										<span class="notice">Allowed File Types: PDF. | Maximum Size : 12 MB.</span>
										<span class="notice">Tip : <a target="_blank" rel="nofollow" href="http://www.freepdfconvert.com/">DOC/DOCX to PDF Conversion</a></span>
										<form data-ajax="false" id="MyUploadForm" enctype="multipart/form-data" method="post" onsubmit="return false" action="http://www.printview.in/wp-content/themes/PrintView/processupload.php">
											<div id="file_up">
												<input type="file" id="imageInput" name="ImageFile">
												<input type="submit" value="Upload" id="submit-btn">
												<img alt="Please Wait" style="display:none;" id="loading-img" src="http://www.printview.in/wp-content/themes/PrintView/images/ajax-loader.gif">
											</div>
											<div style="display:none;" id="progressboxs"><div id="progressbar"></div><div id="statustxt">0%</div></div>
											<div style="display:none; margin-top:10px;" id="countdowntimer"><input type="text" style="display: none" value="180" id="restTime">
												<div id="progressTimer"></div></div>

											<div id="output"><input type="hidden" value="" id="file_loc" name="file_loc"></div>
										</form>
									</div>
									<div id="file_pages"> </div>
									<div class="clearfix"></div>
									<p style="text-align:center; font-style:italic;"><strong>If the Page alignment Of your ".doc" file changes Please Save them as Pdf and then upload. </strong>Don't Know how To convert ? <a style="color:#20ACFB;" target="_blank" href="http://blog.printview.in/how-to-convert-a-word-excel-or-powerpoint-file-to-pdf-using-microsoft-office/">Visit Blog</a> </p>
									<p style="text-align:center; font-style:italic;"><strong>NOTE :</strong> File uploads can take upto 3-5 minutes. Please be patient while file upload is in progress. </p>
									<p style="text-align:center; font-style:italic;"><strong>NOTE :</strong> Orders with invalid or incorrect data may lead to order cancellation. </p>
									<br>
									<a id="goto-2" class="btn btn-block btn-success">Next</a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div style="display:none;" id="step_2">
					<div class="step_2">
						<div class="panel panel-primary">
							<div class="panel-heading"><h3 class="panel-title">Type</h3></div>
							<div class="panel-body text-center">
								<div>
									<input type="radio" checked="" style="display: none" value="BW" id="option2" name="options">
									<label for="option2">
										<div class="color select">
											<i>Black &amp; White</i>
											<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/print-black.jpg">
											<i>Rs. 1.50 /page</i>
										</div>
									</label>
									<input type="radio" style="display: none" value="COLOR" id="option1" name="options">
									<label for="option1">
										<div class="color select">
											<i>Colour</i>
											<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/print-color.jpg">
											<i>Rs. 4.00 /page</i>
										</div>
									</label>
								</div><br>
								<a id="goto-3" class="btn btn-block btn-success">Next</a>

							</div>


						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div style="display:none;" id="step_3" class="">
					<div class="step_3">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Paper Type</h3>
							</div>
							<div class="panel-body text-center">
								<div style="display:none;" id="paper_color">

									<input type="radio" checked="" style="display: none" value="75 GSM(Normal)_0" id="p_type_color0" name="p_type_color">
									<label for="p_type_color0"> <div class="color select">
										<i>75 GSM(Normal)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Included </i>
										</div></label>

									<input type="radio" style="display: none" value="100 GSM (JK Cedar)_0.2" id="p_type_color1" name="p_type_color">
									<label for="p_type_color1"> <div class="color select">
										<i>100 GSM (JK Cedar)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.2 /page</i>
										</div></label>

									<input type="radio" style="display: none" value="80 GSM(Excel Bond)_0.5" id="p_type_color2" name="p_type_color">
									<label for="p_type_color2"> <div class="color select">
										<i>80 GSM(Excel Bond)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.5 /page</i>
										</div></label>

									<input type="radio" style="display: none" value="90 GSM(Excel Bond)_0.6" id="p_type_color3" name="p_type_color">
									<label for="p_type_color3"> <div class="color select">
										<i>90 GSM(Excel Bond)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.6 /page</i>
										</div></label>

									<input type="radio" style="display: none" value="100 GSM (Royal Executive Bond)_0.6" id="p_type_color4" name="p_type_color">
									<label for="p_type_color4"> <div class="color select">
										<i>100 GSM (Royal Executive Bond)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.6 /page</i>
										</div></label>

									<input type="radio" style="display: none" value="180 GSM Premium (Gloss Coated)_8" id="p_type_color5" name="p_type_color">
									<label for="p_type_color5"> <div class="color select">
										<i>180 GSM Premium (Gloss Coated)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 8 /page</i>
										</div></label>
								</div>
								<div style="display:none;" id="paper_bw">

									<input type="radio" style="display: none" checked="" value="75 GSM (Normal)_0" id="p_type_bw0" name="p_type_bw">
									<label for="p_type_bw0"><div class="color select">
										<i>75 GSM (Normal)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Included </i>
										</div></label>

									<input type="radio" style="display: none" value="100 GSM (JK Cedar)_0.2" id="p_type_bw1" name="p_type_bw">
									<label for="p_type_bw1"><div class="color select">
										<i>100 GSM (JK Cedar)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.2 /page</i>
										</div></label>

									<input type="radio" style="display: none" value="80 GSM (Excel Bond)_0.5" id="p_type_bw2" name="p_type_bw">
									<label for="p_type_bw2"><div class="color select">
										<i>80 GSM (Excel Bond)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.5 /page</i>
										</div></label>

									<input type="radio" style="display: none" value="90 GSM (Excel Bond)_0.6" id="p_type_bw3" name="p_type_bw">
									<label for="p_type_bw3"><div class="color select">
										<i>90 GSM (Excel Bond)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.6 /page</i>
										</div></label>

									<input type="radio" style="display: none" value="100 GSM (Royal Executive Bond)_0.6" id="p_type_bw4" name="p_type_bw">
									<label for="p_type_bw4"><div class="color select">
										<i>100 GSM (Royal Executive Bond)</i>
										<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/paper.jpg">
										<i>Rs 0.6 /page</i>
										</div></label>
								</div><br>
								<a id="goto-4" class="btn btn-block btn-success">Next</a>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div style="display:none;" id="step_5" class="">
					<div class="step_3">

						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Binding</h3>
							</div>
							<div class="panel-body text-center">
								<div>
									<input type="radio" checked="" style="display: none" value="none" id="b_option1" name="b_options">
									<label for="b_option1">
										<div class="color select">
											<i>No Binding</i>

											<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/no-binding.jpg">


											<i>included in price</i>
										</div>
									</label>
									<input type="radio" style="display: none" value="spiral" id="b_option2" name="b_options">
									<label for="b_option2">
										<div class="color select">
											<i>Spiral Binding</i>

											<img width="100%" height="100%" src="http://www.printview.in/wp-content/themes/PrintView/images/spiral-binding.png">


											<i>Rs. 25/-</i>
										</div>
									</label>
								</div><br>
								<a id="goto-5" class="btn btn-block btn-success">Next</a>
							</div>
						</div>
						<br>
					</div>
				</div>
				<div class="clearfix"></div>
				<div style="display:none;" id="step_4" class="">
					<div class="step_4">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">No. of Copies</h3>
							</div>
							<div class="panel-body">
								<div class="col-md-6">
									<div class="item_count">
										<div class="form-group">
											<label for="">Number of Copies :</label>
											<input type="text" value="1" placeholder="Number of Copies" id="c_count" name="c_count" class="form-control">
										</div>

									</div>
								</div>
								<div style="padding-top: 20px;" class="col-md-6">
									<span class="cross_X">X</span>
									<span id="count_items" class="count_items">-</span>
								</div>
								<div class="clearfix"></div>
								<div class="price_box">
									<span>Total Cost</span>
									<h4 id="total_price_h4">-</h4>
								</div>
								<div class="clearfix"></div>
								<a id="goto-6" class="btn btn-block btn-success">Calculate Total Charges</a><br>

								<a style="display:none;" id="step_6" class="btn btn-block btn-info">Confirm and Place Order</a>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>

				<div>
					<h3 style="color: #000;text-align: center;font-style: italic;">Order Overview :</h3>
					<table summary="Project Printing Order Overview" id="newspaper-a">

						<tbody>
							<tr>
								<td>Item</td>
								<td>Quality</td>
								<td>Price</td>
								<td>Sub-Total</td>
							</tr>

							<tr>
								<td>Type</td>
								<td>Project</td>
								<td>-</td>
								<td id="page_total">-</td>
							</tr>

							<tr>
								<td>Printing</td>
								<td id="print_type">-</td>
								<td id="print_type_price">-</td>
								<td id="print_type_tprice">-</td>
							</tr>

							<tr>
								<td>Paper Quality</td>
								<td id="paper_type">-</td>
								<td id="paper_type_price">-</td>
								<td id="paper_type_tprice">-</td>
							</tr>

							<tr>
								<td>Binding</td>
								<td id="binding_type">-</td>
								<td id="binding_type_price">-</td>
								<td id="binding_type_tprice">-</td>

							</tr>

							<tr>
								<td>Copies</td>
								<td>-</td>
								<td id="copies_number">-</td>
								<td id="copies_price">-</td>
							</tr>

							<tr>
								<td style="background-color:#A4D483;">-</td>
								<td style="background-color:#A4D483;">-</td>
								<td style="background-color:#A4D483;"><b>Total : </b></td>
								<td id="total_price" style="background-color:#A4D483;">-</td>
							</tr>

						</tbody>
					</table>
				</div>
				<div class="clearfix"></div>
				<div>
				</div>
			</div>
			<div class="col-md-1">
			</div>
			<div class="clearfix"></div>

			<div class="col-md-7 descript">
				<h3>Project Printing</h3>
				<p>PrintView offers online project printing service at affordable rates. We have a large collection of designs and also have several paper options. It’s affordable, trustful and easy service.&nbsp; PrintView’s&nbsp; aim is to bring ideas to life, and our in-depth knowledge and expertise ensures impressive results 100% of the time.&nbsp; We assure high quality, affordable print and design according to your budget and job size.</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<h5 style="text-align: left;" class="p1"><span style="color: #ec008e;"><b></b><br>
					</span></h5>
			</div>
			<div class="col-md-5 image_cont text-center"><br>
				<img width="200" height="225" alt="Project Printing Services India" class="attachment-post-thumbnail wp-post-image" data-lazy-src="http://www.printview.in/wp-content/uploads/2014/07/project-printing.jpg" src="http://www.printview.in/wp-content/plugins/lazy-load/images/1x1.trans.gif"><noscript>&lt;img width="200" height="225" src="http://www.printview.in/wp-content/uploads/2014/07/project-printing.jpg" class="attachment-post-thumbnail wp-post-image" alt="Project Printing Services India" /&gt;</noscript></div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div class="push"></div>

