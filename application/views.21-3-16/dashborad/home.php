 <div class="content">

                  <div class="container">

                        <div class="col-md-8">

                              <h1>Online Printing at Your Door Step</h1>

                              <h3>Design It, Print It & Ship It, anytime, all over India, from projects, Letter Heads, ID Cards to Photos, Brochoures, Envelops etc. </h3>

                              <span class="view_detail"><a href="/shipping/"><h4>Free Standard Shipping on All Orders above Rs. 50/-</h4></a></span>

                              <div class="middle_al">

                                    <span class="purple">
                                          <a href="/products/" class="button"><img src="<?php echo base_url('assets/images/printer11.png'); ?>" width="35"> - Print Now </a>

                                    </span>

                                    <form name="form_track" id="form_track" method="get" action="/my-order/">

                                          <div class="input-group track_order">

                                                <input type="text" class="form-control" name="order_id" id="order_id" placeholder="Track An Order">

                                                <span class="input-group-btn">

                                                      <button class="btn btn-primary" type="submit">

                                                            Track!

                                                      </button>

                                                </span>

                                          </div>

                                    </form>

                              </div>



                        </div>

                        <div class="col-md-4">

                              <img src="<?php echo base_url('assets/images/print_mech.png'); ?>" >

                        </div>





                  </div>

            </div>

            <div class="container text-center block_cont">

                  <div class="col-md-2 col-sm-4">

                        <a href="<?php echo site_url('Products/document_printing'); ?>">

                              <div class="block block_black">

                                    <h2>DOCUMENT PRINTING</h2>

                                    <h3>@ Rs. 1.50/- onwards</h3>

                                    <div class="block_hover">

                                          <p class="btn btn-primary btn-sm" >Order Now!</p>

                                    </div>

                              </div>

                        </a>

                  </div>

                  <div class="col-md-2 col-sm-4">

                        <a href="<?php echo site_url('Products/project_printing'); ?>">

                              <div class="block block_pink">

                                    <h2>PROJECT PRINTING</h2>

                                    <h3>@ Rs. 1.50/- onwards</h3>

                                    <div class="block_hover">

                                          <p class="btn btn-primary btn-sm" >Order Now!</p>

                                    </div>

                              </div>

                        </a>

                  </div>

                  <div class="col-md-2 col-sm-4">

                        <a href="<?php echo site_url('Products/letter_head'); ?>">

                              <div class="block block_yellow">

                                    <h2>LETTER HEAD</h2>

                                    <h3>@ Rs. 3.50/- onwards</h3>

                                    <div class="block_hover">

                                          <p class="btn btn-primary btn-sm">Order Now!</p>

                                    </div>

                              </div>

                        </a>

                  </div>

                  <div class="col-md-2 col-sm-4">

                        <a href="<?php echo site_url('Products/photo_printing'); ?>">

                              <div class="block block_green">

                                    <h2>PHOTO PRINTING</h2>

                                    <h3>@ Re. 1.00/- onwards</h3>

                                    <div class="block_hover">

                                          <p class="btn btn-primary btn-sm">Order Now!</p>

                                    </div>

                              </div>

                        </a>

                  </div>

                  <div class="col-md-2 col-sm-4">

                        <a href="<?php echo site_url('Products/envelope'); ?>">

                              <div class="block block_violet">

                                    <h2>ENVELOPE PRINTING</h2>

                                    <h3>@ Rs. 2.5/- onwards</h3>

                                    <div class="block_hover">

                                          <p class="btn btn-primary btn-sm">Order Now!</p>

                                    </div>

                              </div>

                        </a>

                  </div>

                  <div class="col-md-2 col-sm-4">

                        <a href="<?php echo site_url('Products/visiting_card'); ?>">

                              <div class="block block_brown">

                                    <h2>VISITING CARD</h2>

                                    <h3>@ Rs. 1/- onwards</h3>

                                    <div class="block_hover">

                                          <p class="btn btn-primary btn-sm">Order Now!</p>

                                    </div>

                              </div>

                        </a>

                  </div>

            </div>

            <div class="container" style="margin-top:15px;">

                  <div class="col-md-4 col-sm-4">

                        <img src="<?php echo base_url('assets/images/1.jpg'); ?>" style="width:100%;" >

                  </div>

                  <div class="col-md-4 col-sm-4">

                        <img src="<?php echo base_url('assets/images/2.jpg'); ?>" style="width:100%;" >

                  </div>

                  <div class="col-md-4 col-sm-4">

                        <img src="<?php echo base_url('assets/images/3.jpg'); ?>" style="width:100%;" >

                  </div>

            </div><br>
            <div class="container">
                  <div class="col-md-4 col-sm-4"><img src="<?php echo base_url('assets/images/4.jpg'); ?>" style="width:100%;" ></div>
            <div class="col-md-4 col-sm-4">

                        <img src="<?php echo base_url('assets/images/5.jpg'); ?>" style="width:100%;" >

                  </div>

                  <div class="col-md-4 col-sm-4"><img src="<?php echo base_url('assets/images/6.jpg'); ?>" style="width:100%;" ></div>

            </div>

            <br><br>

            <div class="push"></div>

