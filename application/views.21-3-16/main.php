<?php $this->load->view('common/head' , $header);?>
<body>
      <?php $this->load->view('common/leftbar');?>
      <?php $this->load->view('common/rightbar');?>
      <div id="wrapper">
            <?php $this->load->view('common/header', $header);?>
            <?php $this->load->view($template); ?>
      </div>
      <?php $this->load->view('common/footer');?>



</body>

</html>
