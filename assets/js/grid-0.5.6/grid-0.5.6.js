
if (typeof (gj) === 'undefined') {
    gj = {};
}
if (typeof (gj.grid) === 'undefined') {
    gj.grid = {};
}

gj.grid.configuration = {
    base: {

        dataSource: undefined,

        columns: [],

        autoGenerateColumns: false,

        defaultColumnSettings: {

            hidden: false,

            width: undefined,

            sortable: false,

            type: undefined,

            title: undefined,

            field: undefined,

            align: "left",

            cssClass: undefined,

            headerCssClass: undefined,

            tooltip: undefined,

            icon: undefined,

            format: undefined,

            decimalDigits: undefined,

            tmpl: undefined
        },
        mapping: {

            dataField: "records",

            totalRecordsField: "total"
        },
        params: {},
        defaultParams: {

            sortBy: "sortBy",

            direction: "direction",

            page: "page",

            limit: "limit"
        },

        uiLibrary: "jqueryui",
        style: {
            wrapper: "gj-grid-wrapper",
            table: "gj-grid-table ui-widget-content gj-grid-ui-table",
            loadingCover: "gj-grid-loading-cover",
            loadingText: "gj-grid-loading-text",
            header: {
                cell: "ui-widget-header ui-state-default gj-grid-ui-thead-th",
                sortable: "gj-grid-thead-sortable",
                sortAscIcon: "gj-grid-ui-thead-th-sort-icon ui-icon ui-icon-arrowthick-1-s",
                sortDescIcon: "gj-grid-ui-thead-th-sort-icon ui-icon ui-icon-arrowthick-1-n"
            },
            content: {
                rowHover: "ui-state-hover",
                rowSelected: "ui-state-active"
            },
            pager: {
                cell: "ui-widget-header ui-state-default ui-grid-pager-cell",
                stateDisabled: "ui-state-disabled"
            },

            expandIcon: "ui-icon ui-icon-plus",
            collapseIcon: "ui-icon ui-icon-minus"
        },


        selectionType: 'single',


        selectionMethod: 'basic',


        autoLoad: true,


        notFoundText: "No records found.",


        width: undefined,

        minWidth: undefined,


        fontSize: undefined,

        dataKey: undefined
    },

    bootstrap: {
        style: {
            wrapper: "gj-grid-wrapper",
            table: "gj-grid-table table table-bordered table-hover",
            header: {
                cell: "gj-grid-bootstrap-thead-cell",
                sortable: "gj-grid-thead-sortable",
                sortAscIcon: "glyphicon glyphicon-sort-by-alphabet",
                sortDescIcon: "glyphicon glyphicon-sort-by-alphabet-alt"
            },
            content: {
                rowHover: "",
                rowSelected: "active"
            },
            pager: {
                cell: "gj-grid-bootstrap-tfoot-cell",
                stateDisabled: "ui-state-disabled"
            },
            expandIcon: "glyphicon glyphicon-plus",
            collapseIcon: "glyphicon glyphicon-minus"
        },
        pager: {
            leftControls: [
                $('<button type="button" data-role="page-first" title="First Page" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-step-backward"></span></button>'),
                $('<div>&nbsp;</div>'),
                $('<button type="button" data-role="page-previous" title="Previous Page" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-backward"></span></button>'),
                $('<div>&nbsp;</div>'),
                $('<div>Page</div>'),
                $('<div>&nbsp;</div>'),
                $('<div></div>').append($('<input data-role="page-number" class="form-control input-sm" style="width: 40px; text-align: right;" type="text" value="0">')),
                $('<div>&nbsp;</div>'),
                $('<div>of&nbsp;</div>'),
                $('<div data-role="page-label-last">0</div>'),
                $('<div>&nbsp;</div>'),
                $('<button type="button" data-role="page-next" title="Next Page" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-forward"></span></button>'),
                $('<div>&nbsp;</div>'),
                $('<button type="button" data-role="page-last" title="Last Page" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-step-forward"></span></button>'),
                $('<div>&nbsp;</div>'),
                $('<button type="button" data-role="page-refresh" title="Reload" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-refresh"></span></button>'),
                $('<div>&nbsp;</div>'),
                $('<div></div>').append($('<select data-role="page-size" class="form-control input-sm"></select></div>'))
            ],
            rightControls: [
                $('<div>Displaying records&nbsp;</div>'),
                $('<div data-role="record-first">0</div>'),
                $('<div>&nbsp;-&nbsp;</div>'),
                $('<div data-role="record-last">0</div>'),
                $('<div>&nbsp;of&nbsp;</div>'),
                $('<div data-role="record-total">0</div>').css({ "margin-right": "5px" })
            ]
        }
    }
};


gj.grid.events = {
    beforeEmptyRowInsert: function ($grid, $row) {

        $grid.trigger("beforeEmptyRowInsert", [$row]);
    },
    dataBinding: function ($grid, records) {

        $grid.trigger("dataBinding", [records]);
    },
    dataBound: function ($grid, records, totalRecords) {

        $grid.trigger("dataBound", [records, totalRecords]);
    },
    rowDataBound: function ($grid, $row, id, record) {

        $grid.trigger("rowDataBound", [$row, id, record]);
    },
    cellDataBound: function ($grid, $wrapper, id, column, record) {

        $grid.trigger("cellDataBound", [$wrapper, id, column, record]);
    },
    rowSelect: function ($grid, $row, id, record) {

        $grid.trigger("rowSelect", [$row, id, record]);
    },
    rowUnselect: function ($grid, $row, id, record) {


        $grid.trigger("rowUnselect", [$row, id, record]);

    },
    rowRemoving: function ($grid, $row, id, record) {

        $grid.trigger("rowRemoving", [$row, id, record]);
    },
    destroying: function ($grid) {

        $grid.trigger("destroying");
    },
    columnHide: function ($grid, column) {

        $grid.trigger("columnHide", [column]);
    },
    columnShow: function ($grid, column) {

        $grid.trigger("columnShow", [column]);
    },
    initialized: function ($grid) {

        $grid.trigger("initialized");
    }
};


if (typeof (gj.grid.plugins) === 'undefined') {
    gj.grid.plugins = {};
}

gj.grid.plugins.expandCollapseRows = {
    'configuration': {

        detailTemplate: undefined
    },

    'private': {
        detailExpand: function ($cell, $grid) {
            var $contentRow = $cell.closest("tr"),
                $detailsRow = $('<tr data-role="details"></tr>'),
                $detailsCell = $('<td colspan="' + gj.grid.private.countVisibleColumns($grid) + '"></td>'),
                data = $grid.data('grid'),
                rowData = $contentRow.data('row'),
                $detailWrapper = $(rowData.details);
            $detailsRow.append($detailsCell.append($detailWrapper));
            $detailsRow.insertAfter($contentRow);
            $cell.find('span').attr('class', data.style.collapseIcon);
            $cell.off('click').on('click', function () {
                gj.grid.plugins.expandCollapseRows.private.detailCollapse($(this), $grid);
            });
            gj.grid.plugins.expandCollapseRows.events.detailExpand($grid, $detailWrapper, rowData.record);
        },

        detailCollapse: function ($cell, $grid) {
            var $contentRow = $cell.closest('tr'),
                $detailsRow = $contentRow.next('tr[data-role="details"]'),
                $detailWrapper = $detailsRow.find("td>div");
            $detailsRow.remove();
            $cell.find('span').attr('class', $grid.data('grid').style.expandIcon);
            $cell.off('click').on('click', function () {
                gj.grid.plugins.expandCollapseRows.private.detailExpand($(this), $grid);
            });
            gj.grid.plugins.expandCollapseRows.events.detailCollapse($grid, $detailWrapper, $contentRow.data('row').record);
        },

        updateDetailsColSpan: function ($grid) {
            var $cells = $grid.find('tbody > tr[data-role="details"] > td');
            if ($cells && $cells.length) {
                $cells.attr('colspan', gj.grid.private.countVisibleColumns($grid));
            }
        }
    },

    'public': {
        collapseAll: function () {
            var $grid = this;
            $grid.find('tbody tr[data-role="row"]').each(function () {
                gj.grid.plugins.expandCollapseRows.private.detailCollapse($(this).first(), $grid);
            });
        },

        expandAll: function () {

        }
    },

    'events': {
        detailExpand: function ($grid, $detailWrapper, record) {

            $grid.trigger('detailExpand', [$detailWrapper, record]);
        },
        detailCollapse: function ($grid, $detailWrapper, record) {

            $grid.trigger('detailCollapse', [$detailWrapper, record]);
        }
    },

    'init': function ($grid) {
        $.extend(true, $grid, gj.grid.plugins.expandCollapseRows.public);
        var data = $grid.data('grid');
        if (typeof (data.detailTemplate) !== 'undefined') {
            data.columns = [{
                title: '',
                field: data.dataKey,
                width: (data.uiLibrary === 'jqueryui' ? 24 : 30),
                align: 'center',
                type: 'icon',
                icon: data.style.expandIcon,
                events: {
                    'click': function () {
                        gj.grid.plugins.expandCollapseRows.private.detailExpand($(this), $grid);
                    }
                }
            }].concat(data.columns);

            $grid.on('rowDataBound', function (e, $row, id, record) {
                $row.data('row').details = $(data.detailTemplate);
            });
            $grid.on('columnShow', function (e, column) {
                gj.grid.plugins.expandCollapseRows.private.updateDetailsColSpan($grid);
            });
            $grid.on('columnHide', function (e, column) {
                gj.grid.plugins.expandCollapseRows.private.updateDetailsColSpan($grid);
            });
            $grid.on('rowRemoving', function (e, $row, id, record) {
                gj.grid.plugins.expandCollapseRows.private.detailCollapse($row.children('td').first(), $grid);
            });
            $grid.on('pageChanging', function () {
                $grid.collapseAll();
            });
        }
    }
};

$.extend(true, gj.grid.configuration.base, gj.grid.plugins.expandCollapseRows.configuration);

if (typeof (gj.grid.plugins) === 'undefined') {
    gj.grid.plugins = {};
}

gj.grid.plugins.inlineEditing = {
    'configuration': {
        defaultColumnSettings: {

            editor: undefined
        }
    },

    'private': {
        OnCellEdit: function ($grid, $cell, column, record) {
            var $editorContainer, $editorField;
            if ($cell.attr('data-mode') !== 'edit' && column.editor) {
                $cell.find('div[data-role="display"]').hide();
                $editorContainer = $cell.find('div[data-role="edit"]');
                if ($editorContainer && $editorContainer.length) {
                    $editorContainer.show();
                    $editorField = $editorContainer.find('input, select').first();
                    $editorField.val(record[column.field]);
                } else {
                    $editorContainer = $('<div data-role="edit" />');
                    $cell.append($editorContainer);
                    if (typeof (column.editor) === 'function') {
                        column.editor($editorContainer, record[column.field]);
                    } else if (typeof (column.editor) === 'boolean') {
                        $editorContainer.append('<input type="text" value="' + record[column.field] + '"/>');
                    }
                    $editorField = $editorContainer.find('input, select').first();
                    $editorField.on('blur', function (e) {
                        gj.grid.plugins.inlineEditing.private.OnCellDisplay($grid, $cell, column);
                    });
                    $editorField.on('keypress', function (e) {
                        if (e.which === 13) {
                            gj.grid.plugins.inlineEditing.private.OnCellDisplay($grid, $cell, column);
                        }
                    });
                }
                $editorField.focus().select();
                $cell.attr('data-mode', 'edit');
            }
        },

        OnCellDisplay: function ($grid, $cell, column) {
            var newValue, oldValue, record, style = '';
            if ($cell.attr('data-mode') === 'edit') {
                $editorContainer = $cell.find('div[data-role="edit"]');
                newValue = $editorContainer.find('input, select').first().val();
                record = $cell.parent().data('row').record;
                oldValue = record[column.field];
                $displayContainer = $cell.find('div[data-role="display"]');
                if (newValue !== oldValue) {
                    gj.grid.private.setCellText($displayContainer, column, newValue);
                    record[column.field] = newValue;
                    if ($cell.find('span.gj-dirty').length === 0) {
                        if ($cell.css('padding-top') !== '0px') {
                            style += 'margin-top: -' + $cell.css('padding-top') + ';';
                        }
                        if ($cell.css('padding-left') !== '0px') {
                            style += 'margin-left: -' + $cell.css('padding-left') + ';';
                        }
                        style = style ? ' style="' + style + '"' : '';
                        $cell.prepend($('<span class="gj-dirty"' + style + '></span>'));
                    }
                    $cell.attr('data-mode', 'display');
                    gj.grid.plugins.inlineEditing.events.cellDataChanged($grid, $cell, column, record, oldValue, newValue);
                    gj.grid.plugins.inlineEditing.private.updateChanges($grid, column, record, newValue);
                }
                $editorContainer.hide();
                $displayContainer.show();
            }
        },

        updateChanges: function ($grid, column, sourceRecord, newValue) {
            var targetRecords, filterResult, newRecord, data = $grid.data('grid');
            if (!data.guid) {
                data.guid = gj.grid.plugins.inlineEditing.private.generateGUID();
            }
            if (data.dataKey) {
                targetRecords = JSON.parse(sessionStorage.getItem('gj.grid.' + data.guid));
                if (targetRecords) {
                    filterResult = targetRecords.filter(function (record) {
                        return record[data.dataKey] === sourceRecord[data.dataKey];
                    });
                } else {
                    targetRecords = [];
                }
                if (filterResult && filterResult.length === 1) {
                    filterResult[0][column.field] = newValue;
                } else {
                    newRecord = {};
                    newRecord[data.dataKey] = sourceRecord[data.dataKey];
                    if (data.dataKey !== column.field) {
                        newRecord[column.field] = newValue;
                    }
                    targetRecords.push(newRecord);
                }
                sessionStorage.setItem('gj.grid.' + data.guid, JSON.stringify(targetRecords));
            }
        },

        generateGUID: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }
    },

    'public': {

        getChanges: function () {
            return JSON.parse(sessionStorage.getItem('gj.grid.' + this.data('grid').guid));
        }
    },

    'events': {
        cellDataChanged: function ($grid, $cell, column, record, oldValue, newValue) {

            $grid.trigger('cellDataChanged', [$cell, column, record, oldValue, newValue]);
        }
    },

    'init': function ($grid) {
        $.extend(true, $grid, gj.grid.plugins.inlineEditing.public);
        $grid.on('cellDataBound', function (e, $wrapper, id, column, record) {
            if (column.editor) {
                $wrapper.parent().on('click', function () {
                    gj.grid.plugins.inlineEditing.private.OnCellEdit($grid, $wrapper.parent(), column, record);
                });
            }
        });

    }
};

$.extend(true, gj.grid.configuration.base, gj.grid.plugins.inlineEditing.configuration);

if (typeof (gj.grid.plugins) === 'undefined') {
    gj.grid.plugins = {};
}

gj.grid.plugins.pagination = {
    'configuration': {
        pager: {

            enable: false,


            limit: 10,


            sizes: undefined,


            leftControls: [
                $('<div title="First" data-role="page-first" class="ui-icon ui-icon-seek-first ui-grid-icon"></div>'),
                $('<div title="Previous" data-role="page-previous" class="ui-icon ui-icon-seek-prev ui-grid-icon"></div>'),
                $('<div>Page</div>'),
                $('<div></div>').append($('<input type="text" data-role="page-number" class="ui-grid-pager" value="0">')),
                $('<div>of&nbsp;</div>'),
                $('<div data-role="page-label-last">0</div>'),
                $('<div title="Next" data-role="page-next" class="ui-icon ui-icon-seek-next ui-grid-icon"></div>'),
                $('<div title="Last" data-role="page-last" class="ui-icon ui-icon-seek-end ui-grid-icon"></div>'),
                $('<div title="Reload" data-role="page-refresh" class="ui-icon ui-icon-refresh ui-grid-icon"></div>'),
                $('<div></div>').append($('<select data-role="page-size" class="ui-grid-page-sizer"></select>'))
            ],


            rightControls: [
                $('<div>Displaying records&nbsp;</div>'),
                $('<div data-role="record-first">0</div>'),
                $('<div>&nbsp;-&nbsp;</div>'),
                $('<div data-role="record-last">0</div>'),
                $('<div>&nbsp;of&nbsp;</div>'),
                $('<div data-role="record-total">0</div>').css({ "margin-right": "5px" })
            ]
        }
    },

    'private': {
        init: function ($grid) {
            var $row, $cell, data, controls, $leftPanel, $rightPanel, $tfoot, leftControls, rightControls, i;

            data = $grid.data('grid');

            if (data.pager && data.pager.enable) {

                data.params[data.defaultParams.page] = 1;
                data.params[data.defaultParams.limit] = data.pager.limit;

                $row = $('<tr/>');
                $cell = $('<th/>').addClass(data.style.pager.cell);
                $row.append($cell);

                $leftPanel = $('<div />').css({ 'float': 'left' });
                $rightPanel = $('<div />').css({ 'float': 'right' });
                if (/msie/.test(navigator.userAgent.toLowerCase())) {
                    $rightPanel.css({ 'padding-top': '3px' });
                }

                $cell.append($leftPanel).append($rightPanel);

                $tfoot = $('<tfoot />').append($row);
                $grid.append($tfoot);
                gj.grid.plugins.pagination.private.updatePagerColSpan($grid);

                leftControls = gj.grid.private.clone(data.pager.leftControls);
                $.each(leftControls, function () {
                    $leftPanel.append(this);
                });

                rightControls = gj.grid.private.clone(data.pager.rightControls);
                $.each(rightControls, function () {
                    $rightPanel.append(this);
                });

                controls = $grid.find('TFOOT [data-role]');
                for (i = 0; i < controls.length; i++) {
                    gj.grid.plugins.pagination.private.initPagerControl($(controls[i]), $grid);
                }
            }
        },

        initPagerControl: function ($control, $grid) {
            var data = $grid.data('grid');
            switch ($control.data('role')) {
                case 'page-number':
                    $control.on('keypress', function (e) {
                        if (e.keyCode === 13) {
                            $(this).trigger('change');
                        }
                    });
                    break;
                case 'page-size':
                    if (data.pager.sizes && 0 < data.pager.sizes.length) {
                        $control.show();
                        $.each(data.pager.sizes, function () {
                            $control.append($('<option/>').attr('value', this.toString()).text(this.toString()));
                        });
                        $control.change(function () {
                            var newSize = parseInt(this.value, 10);
                            data.params[data.defaultParams.limit] = newSize;
                            gj.grid.plugins.pagination.private.changePage($grid, 1);
                            gj.grid.plugins.pagination.events.pageSizeChange($grid, newSize);
                        });
                        $control.val(data.params[data.defaultParams.limit]);
                    } else {
                        $control.hide();
                    }
                    break;
                case 'page-refresh':
                    $control.on('click', function () { $grid.reload(); });
                    break;
            }

        },

        reloadPager: function ($grid, totalRecords) {
            var page, limit, lastPage, firstRecord, lastRecord, data;

            data = $grid.data('grid');

            if (data.pager.enable) {
                page = (0 === totalRecords) ? 0 : data.params[data.defaultParams.page];
                limit = parseInt(data.params[data.defaultParams.limit], 10);
                lastPage = Math.ceil(totalRecords / limit);
                firstRecord = (0 === page) ? 0 : (limit * (page - 1)) + 1;
                lastRecord = (firstRecord + limit) > totalRecords ? totalRecords : (firstRecord + limit) - 1;

                controls = $grid.find('TFOOT [data-role]');
                for (i = 0; i < controls.length; i++) {
                    gj.grid.plugins.pagination.private.reloadPagerControl($(controls[i]), $grid, page, lastPage, firstRecord, lastRecord, totalRecords);
                }

                gj.grid.plugins.pagination.private.updatePagerColSpan($grid);
            }
        },

        reloadPagerControl: function ($control, $grid, page, lastPage, firstRecord, lastRecord, totalRecords) {
            var data = $grid.data('grid');
            switch ($control.data('role')) {
                case 'page-first':
                    if (page < 2) {
                        $control.addClass(data.style.pager.stateDisabled).off('click');
                    } else {
                        $control.removeClass(data.style.pager.stateDisabled).off('click').on('click', gj.grid.plugins.pagination.private.CreateFirstPageHandler($grid));
                    }
                    break;
                case 'page-previous':
                    if (page < 2) {
                        $control.addClass(data.style.pager.stateDisabled).off('click');
                    } else {
                        $control.removeClass(data.style.pager.stateDisabled).off('click').on('click', gj.grid.plugins.pagination.private.CreatePrevPageHandler($grid));
                    }
                    break;
                case 'page-number':
                    $control.val(page).off('change').on('change', gj.grid.plugins.pagination.private.CreateChangePageHandler($grid, page, lastPage));
                    break;
                case 'page-label-last':
                    $control.text(lastPage);
                    break;
                case 'page-next':
                    if (lastPage === page) {
                        $control.addClass(data.style.pager.stateDisabled).off('click');
                    } else {
                        $control.removeClass(data.style.pager.stateDisabled).off('click').on('click', gj.grid.plugins.pagination.private.CreateNextPageHandler($grid));
                    }
                    break;
                case 'page-last':
                    if (lastPage === page) {
                        $control.addClass(data.style.pager.stateDisabled).off('click');
                    } else {
                        $control.removeClass(data.style.pager.stateDisabled).off('click').on('click', gj.grid.plugins.pagination.private.CreateLastPageHandler($grid, lastPage));
                    }
                    break;
                case 'record-first':
                    $control.text(firstRecord);
                    break;
                case 'record-last':
                    $control.text(lastRecord);
                    break;
                case 'record-total':
                    $control.text(totalRecords);
                    break;
            }
        },

        CreateFirstPageHandler: function ($grid) {
            return function () {
                gj.grid.plugins.pagination.private.changePage($grid, 1);
            };
        },

        CreatePrevPageHandler: function ($grid) {
            return function () {
                var data = $grid.data('grid'),
                    currentPage = data.params[data.defaultParams.page],
                    newPage = (currentPage && currentPage > 1) ? currentPage - 1 : 1;
                gj.grid.plugins.pagination.private.changePage($grid, newPage);
            };
        },

        CreateNextPageHandler: function ($grid) {
            return function () {
                var data = $grid.data('grid'),
                    currentPage = data.params[data.defaultParams.page];
                gj.grid.plugins.pagination.private.changePage($grid, currentPage + 1);
            };
        },

        CreateLastPageHandler: function ($grid, lastPage) {
            return function () {
                gj.grid.plugins.pagination.private.changePage($grid, lastPage);
            };
        },

        CreateChangePageHandler: function ($grid, currentPage, lastPage) {
            return function (e) {
                var data = $grid.data('grid'),
                    newPage = parseInt(this.value, 10);
                if (newPage && !isNaN(newPage) && newPage <= lastPage) {
                    gj.grid.plugins.pagination.private.changePage($grid, newPage);
                } else {
                    this.value = currentPage;
                    alert('Please enter a valid number.');
                }
            };
        },

        changePage: function ($grid, newPage) {
            var data = $grid.data('grid');
            $grid.find('TFOOT [data-role="page-number"]').val(newPage);
            data.params[data.defaultParams.page] = newPage;
            gj.grid.plugins.pagination.events.pageChanging($grid, newPage);
            $grid.reload();
        },

        updatePagerColSpan: function ($grid) {
            var $cell = $grid.find('tfoot > tr > th');
            if ($cell && $cell.length) {
                $cell.attr('colspan', gj.grid.private.countVisibleColumns($grid));
            }
        }
    },

    'public': {
    },

    'events': {

        pageSizeChange: function ($grid, newSize) {
            $grid.trigger('pageSizeChange', [newSize]);
        },


        pageChanging: function ($grid, newSize) {
            $grid.trigger('pageChanging', [newSize]);
        }
    },

    'init': function ($grid) {
        gj.grid.plugins.pagination.private.init($grid);
        $grid.on('dataBound', function (e, records, totalRecords) {
            gj.grid.plugins.pagination.private.reloadPager($grid, totalRecords);
        });
        $grid.on('columnShow', function (e, column) {
            gj.grid.plugins.pagination.private.updatePagerColSpan($grid);
        });
        $grid.on('columnHide', function (e, column) {
            gj.grid.plugins.pagination.private.updatePagerColSpan($grid);
        });
    }
};

$.extend(true, gj.grid.configuration.base, gj.grid.plugins.pagination.configuration);


if (typeof (gj.grid.plugins) === 'undefined') {
    gj.grid.plugins = {};
}

gj.grid.plugins.responsiveDesign = {
    'configuration': {
        base: {

            resizeCheckInterval: 500,

            responsive: false,

            showHiddenColumnsAsDetails: false,
            defaultColumn: {

                priority: undefined,

                minWidth: 150
            },
            style: {
                rowDetailItem: ''
            }
        },

        bootstrap: {
            style: {
                rowDetailItem: 'col-lg-4'
            }
        }
    },

    'private': {

        orderColumns: function (config) {
            var result = [];
            if (config.columns && config.columns.length) {
                for (i = 0; i < config.columns.length; i++) {
                    result.push({
                        position: i,
                        field: config.columns[i].field,
                        minWidth: config.columns[i].width || config.columns[i].minWidth || config.defaultColumn.minWidth,
                        priority: config.columns[i].priority || 0
                    });
                }
                result.sort(function (a, b) {
                    var result = 0;
                    if (a.priority < b.priority) {
                        result = -1;
                    } else if (a.priority > b.priority) {
                        result = 1;
                    }
                    return result;
                });
            }
            return result;
        },

        updateDetails: function ($grid) {
            var $rows, data, i, j, $row, rowData, $placeholder, column, text;
            $rows = $grid.find('tbody > tr');
            data = $grid.data('grid');
            for (i = 0; i < $rows.length; i++) {
                $row = $($rows[i]);
                if ($row.data('role') === 'row') {
                    rowData = $row.data('row');
                    for (j = 0; j < data.columns.length; j++) {
                        column = data.columns[j];
                        $placeholder = rowData.details && rowData.details.find('div[data-id="' + column.field + '"]');
                        if (data.columns[j].hidden) {
                            text = gj.grid.private.formatText(rowData.record[column.field], column);
                            if (!$placeholder || !$placeholder.length) {
                                $placeholder = $('<div data-id="' + column.field + '"><b>' + (column.title || column.field) + '</b>: ' + text + '</div>');
                                $placeholder.addClass(data.style.rowDetailItem);
                                if (!rowData.details || !rowData.details.length) {
                                    rowData.details = $('<div/>');
                                }
                                rowData.details.append($placeholder);
                            } else {
                                $placeholder.html('<b>' + column.title + '</b>: ' + text);
                            }
                        } else if ($placeholder && $placeholder.length) {
                            $placeholder.remove();
                        }
                    }
                }
            }
        }
    },

    'public': {

        oldWidth: undefined,

        resizeCheckIntervalId: undefined,


        makeResponsive: function () {
            var i, $column,
                extraWidth = 0,
                config = this.data('grid'),
                columns = gj.grid.plugins.responsiveDesign.private.orderColumns(config);

            for (i = 0; i < columns.length; i++) {
                $column = this.find('thead>tr>th:eq(' + columns[i].position + ')');
                if ($column.is(':visible') && columns[i].minWidth < $column.width()) {
                    extraWidth += $column.width() - columns[i].minWidth;
                }
            }

            if (extraWidth) {
                for (i = 0; i < columns.length; i++) {
                    $column = this.find('thead>tr>th:eq(' + columns[i].position + ')');
                    if (!$column.is(':visible') && columns[i].minWidth <= extraWidth) {
                        this.showColumn(columns[i].field);
                        extraWidth -= $column.width();
                    }
                }
            }

            for (i = (columns.length - 1); i >= 0; i--) {
                $column = this.find('thead>tr>th:eq(' + columns[i].position + ')');
                if ($column.is(':visible') && columns[i].priority && columns[i].minWidth > $column.outerWidth()) {
                    this.hideColumn(columns[i].field);
                }
            }
        },
    },

    'events': {
        resize: function ($grid, newWidth, oldWidth) {

            $grid.trigger('resize', [newWidth, oldWidth]);
        }
    },

    'init': function ($grid) {
        $.extend(true, $grid, gj.grid.plugins.responsiveDesign.public);
        var data = $grid.data('grid');
        if (data.responsive) {
            $grid.on('initialized', function () {
                $grid.makeResponsive();
                $grid.oldWidth = $grid.width();
                $grid.resizeCheckIntervalId = setInterval(function () {
                    var newWidth = $grid.width();
                    if (newWidth !== $grid.oldWidth) {
                        gj.grid.plugins.responsiveDesign.events.resize($grid, newWidth, $grid.oldWidth);
                    }
                    $grid.oldWidth = newWidth;
                }, data.resizeCheckInterval);
            });
            $grid.on('destroy', function () {
                if ($grid.resizeCheckIntervalId) {
                    clearInterval($grid.resizeCheckIntervalId);
                }
            });
            $grid.on('resize', function () {
                $grid.makeResponsive();
            });
        }
        if (data.showHiddenColumnsAsDetails && gj.grid.plugins.expandCollapseRows) {
            $grid.on('dataBound', function () {
                gj.grid.plugins.responsiveDesign.private.updateDetails($grid);
            });
            $grid.on('columnHide', function () {
                gj.grid.plugins.responsiveDesign.private.updateDetails($grid);
            });
            $grid.on('columnShow', function () {
                gj.grid.plugins.responsiveDesign.private.updateDetails($grid);
            });
            $grid.on('rowDataBound', function () {
                gj.grid.plugins.responsiveDesign.private.updateDetails($grid);
            });
        }
    }
};

$.extend(true, gj.grid.configuration, gj.grid.plugins.responsiveDesign.configuration);


if (typeof (gj.grid.plugins) === 'undefined') {
    gj.grid.plugins = {};
}

gj.grid.plugins.toolbar = {
    'configuration': {
        base: {

            toolbarTemplate: undefined,


            title: undefined,

            style: {
                toolbar: "ui-widget-header ui-state-default gj-grid-ui-toolbar"
            }
        },

        bootstrap: {
            style: {
                toolbar: "gj-grid-bootstrap-toolbar"
            }
        }
    },

    'private': {
    },

    'public': {

        title: function (text) {
            var $titleEl = this.parent().find('div[data-role="toolbar"] [data-role="title"]');
            if (typeof (text) !== 'undefined') {
                $titleEl.text(text);
                return this;
            } else {
                return $titleEl.text();
            }
        }
    },

    'init': function ($grid) {
        var data, $toolbar, $title;
        $.extend(true, $grid, gj.grid.plugins.toolbar.public);
        data = $grid.data('grid');
        $toolbar = $grid.prev('div[data-role="toolbar"]');
        if (typeof (data.toolbarTemplate) !== 'undefined' || typeof (data.title) !== 'undefined' || $toolbar.length > 0) {
            if ($toolbar.length === 0) {
                $toolbar = $('<div data-role="toolbar"></div>');
                $grid.before($toolbar);
            }
            $toolbar.addClass(data.style.toolbar);

            if ($toolbar.children().length === 0 && data.toolbarTemplate) {
                $toolbar.append(data.toolbarTemplate);
            }

            $title = $toolbar.find('[data-role="title"]');
            if ($title.length === 0) {
                $title = $('<div data-role="title"/>');
                $toolbar.prepend($title);
            }
            if (data.title) {
                $title.text(data.title);
            }

            if (data.minWidth) {
                $toolbar.css("min-width", data.minWidth);
            }
        }
    }
};

$.extend(true, gj.grid.configuration, gj.grid.plugins.toolbar.configuration);

gj.grid.private = {

    init: function (jsConfig) {
        var plugin, option, data = this.data('grid');
        if (!data) {
            gj.grid.private.SetOptions(this, jsConfig || {});
            gj.grid.private.InitGrid(this);

            for (plugin in gj.grid.plugins) {
                if (gj.grid.plugins.hasOwnProperty(plugin)) {
                    gj.grid.plugins[plugin].init(this);
                }
            }
            data = this.data('grid');

            for (option in data) {
                if (gj.grid.events.hasOwnProperty(option)) {
                    this.on(option, data[option]);
                    delete data[option];
                }
            }
            gj.grid.private.HeaderRenderer(this);
            gj.grid.private.AppendEmptyRow(this, "&nbsp;");
            gj.grid.events.initialized(this);
            if (data.autoLoad) {
                this.reload();
            }
        }
        return this;
    },

    SetOptions: function ($grid, jsConfig) {
        var options = $.extend(true, {}, gj.grid.configuration.base),
            htmlConfig = gj.grid.private.getHTMLConfiguration($grid);
        if ((jsConfig.uiLibrary && jsConfig.uiLibrary === 'bootstrap') || (htmlConfig.uiLibrary && htmlConfig.uiLibrary === 'bootstrap')) {
            $.extend(true, options, gj.grid.configuration.bootstrap);
        }
        $.extend(true, options, htmlConfig);
        $.extend(true, options, jsConfig);
        gj.grid.private.setDefaultColumnConfig(options.columns, options.defaultColumnSettings);
        $grid.data('grid', options);
    },

    setDefaultColumnConfig: function (columns, defaultColumnSettings) {
        var column;
        if (columns && columns.length) {
            for (i = 0; i < columns.length; i++) {
                column = $.extend(true, {}, defaultColumnSettings);
                $.extend(true, column, columns[i]);
                columns[i] = column;
            }
        }
    },

    getHTMLConfiguration: function ($grid) {
        var result = gj.grid.private.getAttributes($grid);
        if (result && result.source) {
            result.dataSource = result.source;
            delete result.source;
        }
        result.columns = [];
        $grid.find('thead > tr > th').each(function () {
            var $el = $(this),
                title = $el.text(),
                config = gj.grid.private.getAttributes($el);
            config.title = title;
            if (!config.field) {
                config.field = title;
            }
            if (config.events) {
                config.events = gj.grid.private.eventsParser(config.events);
            }
            result.columns.push(config);
        });
        return result;
    },

    getAttributes: function ($el) {
        var result = $el.data(),
            width = $el.attr('width');
        if (width) {
            result.width = width;
        }
        return result;
    },

    eventsParser: function (events) {
        var result = {}, list, i, key, func, position;
        list = events.split(',');
        for (i = 0; i < list.length; i++) {
            position = list[i].indexOf(':');
            if (position > 0) {
                key = $.trim(list[i].substr(0, position));
                func = $.trim(list[i].substr(position + 1, list[i].length));
                result[key] = eval('window.' + func);
            }
        }
        return result;
    },

    LoaderSuccessHandler: function ($grid) {
        return function (response) {
            $grid.render(response);
        };
    },

    InitGrid: function ($grid) {
        var data = $grid.data('grid'),
            $wrapper = $grid.parent('div[data-role="wrapper"]');

        if ($wrapper.length === 0) {
            $wrapper = $('<div data-role="wrapper" />').addClass(data.style.wrapper);
            $grid.wrap($wrapper);
        } else {
            $wrapper.addClass(data.style.wrapper);
        }

        if (data.width) {
            $grid.parent().css("width", data.width);
        }
        if (data.minWidth) {
            $grid.css("min-width", data.minWidth);
        }
        if (data.fontSize) {
            $grid.css("font-size", data.fontSize);
        }
        $grid.addClass(data.style.table);
        if ("checkbox" === data.selectionMethod) {
            data.columns = [{ title: '', field: data.dataKey, width: (data.uiLibrary === 'jqueryui' ? 24 : 30), align: 'center', type: 'checkbox' }].concat(data.columns);
        }
        $grid.append($("<tbody/>"));
    },

    HeaderRenderer: function ($grid) {
        var data, columns, style, sortBy, direction, $thead, $row, $cell, i, $checkAllBoxes;

        data = $grid.data('grid');
        columns = data.columns;
        style = data.style.header;
        sortBy = data.params[data.defaultParams.sortBy];
        direction = data.params[data.defaultParams.direction];

        $thead = $grid.children('thead');
        if ($thead.length === 0) {
            $thead = $('<thead />');
            $grid.prepend($thead);
        }

        $row = $('<tr/>');
        for (i = 0; i < columns.length; i += 1) {
            $cell = $('<th/>');
            if (columns[i].width) {
                $cell.attr('width', columns[i].width);
            }
            $cell.addClass(style.cell);
            if (columns[i].headerCssClass) {
                $cell.addClass(columns[i].headerCssClass);
            }
            $cell.css('text-align', columns[i].align || 'left');
            if (columns[i].sortable) {
                $cell.addClass(style.sortable);
                $cell.on('click', gj.grid.private.CreateSortHandler($grid, $cell));
            }
            if ('checkbox' === data.selectionMethod && 'multiple' === data.selectionType && 'checkbox' === columns[i].type) {
                $checkAllBoxes = $cell.find('input[id="checkAllBoxes"]');
                if ($checkAllBoxes.length === 0) {
                    $checkAllBoxes = $('<input type="checkbox" id="checkAllBoxes" />');
                    $cell.append($checkAllBoxes);
                }
                $checkAllBoxes.hide().off('click').on('click', function () {
                    if (this.checked) {
                        $grid.selectAll();
                    } else {
                        $grid.unSelectAll();
                    }
                });
            } else {
                $cell.append($('<div style="float: left"/>').text(typeof (columns[i].title) === 'undefined' ? columns[i].field : columns[i].title));
            }
            if (columns[i].hidden) {
                $cell.hide();
            }

            $cell.data('cell', columns[i]);
            $row.append($cell);
        }

        $thead.empty().append($row);
    },

    CreateSortHandler: function ($grid, $cell) {
        return function () {
            var $sortIcon, data, cellData, style, params = {};
            if ($grid.count() > 0) {
                data = $grid.data('grid');
                cellData = $cell.data('cell');
                cellData.direction = (cellData.direction === 'asc' ? 'desc' : 'asc');
                params[data.defaultParams.sortBy] = cellData.field;
                params[data.defaultParams.direction] = cellData.direction;

                style = data.style.header;
                $cell.siblings().find('span[data-role="sorticon"]').remove();
                $sortIcon = $cell.children('span[data-role="sorticon"]');
                if ($sortIcon.length === 0) {
                    $sortIcon = $('<span data-role="sorticon" style="float: left; margin-left:5px;"/>');
                    $cell.append($sortIcon);
                } else {
                    $sortIcon.removeClass('asc' === cellData.direction ? style.sortDescIcon : style.sortAscIcon);
                }
                $sortIcon.addClass('asc' === cellData.direction ? style.sortAscIcon : style.sortDescIcon);

                $grid.reload(params);
            }
        };
    },

    StartLoading: function ($grid) {
        var $tbody, $cover, $loading, width, height, top, data;
        gj.grid.private.StopLoading($grid);
        data = $grid.data('grid');
        if (0 === $grid.outerHeight()) {
            return;
        }
        $tbody = $grid.children("tbody");
        width = $tbody.outerWidth(false);
        height = $tbody.outerHeight(false);
        top = $tbody.prev().outerHeight(false) + $grid.prevAll().outerHeight(false) + parseInt($grid.parent().css("padding-top").replace('px', ''), 10);
        $cover = $("<div data-role='loading-cover' />").addClass(data.style.loadingCover).css({
            width: width,
            height: height,
            top: top
        });
        $loading = $("<div data-role='loading-text'>Loading...</div>").addClass(data.style.loadingText);
        $loading.insertAfter($grid);
        $cover.insertAfter($grid);
        $loading.css({
            top: top + (height / 2) - ($loading.outerHeight(false) / 2),
            left: (width / 2) - ($loading.outerWidth(false) / 2)
        });
    },

    StopLoading: function ($grid) {
        $grid.parent().find("div[data-role='loading-cover']").remove();
        $grid.parent().find("div[data-role='loading-text']").remove();
    },

    CreateAddRowHoverHandler: function ($row, cssClass) {
        return function () {
            $row.addClass(cssClass);
        };
    },

    CreateRemoveRowHoverHandler: function ($row, cssClass) {
        return function () {
            $row.removeClass(cssClass);
        };
    },

    AppendEmptyRow: function ($grid, caption) {
        var data, $row, $cell, $wrapper;
        data = $grid.data('grid');
        $row = $('<tr data-role="empty"/>');
        $cell = $('<td/>').css({ 'width': '100%', 'text-align': 'center' });
        $cell.attr('colspan', gj.grid.private.countVisibleColumns($grid));
        $wrapper = $('<div />').html(caption || data.notFoundText);
        $cell.append($wrapper);
        $row.append($cell);

        gj.grid.events.beforeEmptyRowInsert($grid, $row);

        $grid.append($row);
    },

    autoGenerateColumns: function ($grid, records) {
        var names, value, type,
            data = $grid.data('grid');
        data.columns = [];
        if (records.length > 0) {
            names = Object.getOwnPropertyNames(records[0]);
            for (i = 0; i < names.length; i++) {
                value = records[0][names[i]];
                type = 'text';
                if (value) {
                    if (value.indexOf('/Date(') > -1) {
                        type = 'date';
                    } else if (typeof value === 'number') {
                        type = 'number';
                    }
                }
                data.columns.push({ field: names[i], type: type });
            }
            gj.grid.private.setDefaultColumnConfig(data.columns, data.defaultColumnSettings);
        }
        gj.grid.private.HeaderRenderer($grid);
    },

    loadData: function ($grid, records, totalRecords) {
        var data, records, i, j, recLen, rowCount,
            $tbody, $rows, $row, $checkAllBoxes;

        gj.grid.events.dataBinding($grid, records);
        data = $grid.data('grid');
        recLen = records.length;
        gj.grid.private.StopLoading($grid);

        if (data.autoGenerateColumns) {
            gj.grid.private.autoGenerateColumns($grid, records);
        }

        $tbody = $grid.find('tbody');
        if ('checkbox' === data.selectionMethod && 'multiple' === data.selectionType) {
            $checkAllBoxes = $grid.find('input#checkAllBoxes');
            $checkAllBoxes.prop('checked', false);
            if (0 === recLen) {
                $checkAllBoxes.hide();
            } else {
                $checkAllBoxes.show();
            }
        }
        $tbody.find('tr[data-role="empty"]').remove();
        if (0 === recLen) {
            $tbody.empty();
            gj.grid.private.AppendEmptyRow($grid);
        }

        $rows = $tbody.children('tr');
        rowCount = $rows.length;

        for (i = 0; i < rowCount; i++) {
            if (i < recLen) {
                $row = $rows.eq(i);
                gj.grid.private.RowRenderer($grid, $row, records[i], i);
            } else {
                $tbody.find('tr:gt(' + (i - 1) + ')').remove();
                break;
            }
        }

        for (i = rowCount; i < recLen; i++) {
            gj.grid.private.RowRenderer($grid, null, records[i], i);
        }
        gj.grid.events.dataBound($grid, records, totalRecords);
    },

    RowRenderer: function ($grid, $row, record, position) {
        var id, $cell, i, data, mode;
        data = $grid.data('grid');
        if (!$row || $row.length === 0) {
            mode = "create";
            $row = $($grid.find("tbody")[0].insertRow(position));
            $row.attr('data-role', 'row');
            $row.bind({
                "mouseenter.grid": gj.grid.private.CreateAddRowHoverHandler($row, data.style.content.rowHover),
                "mouseleave.grid": gj.grid.private.CreateRemoveRowHoverHandler($row, data.style.content.rowHover)
            });
        } else {
            mode = "update";
            $row.removeClass(data.style.content.rowSelected).off("click");
        }
        id = (data.dataKey && record[data.dataKey]) ? record[data.dataKey] : (position + 1);
        $row.data("row", { id: id, record: record });
        $row.on("click", gj.grid.private.CreateRowClickHandler($grid, id, record));
        for (i = 0; i < data.columns.length; i++) {
            if (mode === "update") {
                $cell = $row.find("td:eq(" + i + ")");
                gj.grid.private.CellRenderer($grid, $cell, data.columns[i], record, id);
            } else {
                $cell = gj.grid.private.CellRenderer($grid, null, data.columns[i], record, id);
                $row.append($cell);
            }
        }
        gj.grid.events.rowDataBound($grid, $row, id, record);
    },

    CellRenderer: function ($grid, $cell, column, record, id, mode) {
        var text, $wrapper, mode, $icon, data;

        data = $grid.data('grid');

        if (!$cell || $cell.length === 0) {
            $cell = $('<td/>').css('text-align', column.align || 'left');
            $wrapper = $('<div data-role="display" />');
            if (column.cssClass) {
                $cell.addClass(column.cssClass);
            }
            $cell.append($wrapper);
            mode = 'create';
        } else {
            $wrapper = $cell.find('div[data-role="display"]');
            mode = 'update';
        }

        if ('checkbox' === column.type) {
            if ('create' === mode) {
                $wrapper.append($('<input />').attr('type', 'checkbox').val(id));
            } else {
                $wrapper.find('input[type="checkbox"]').val(id).prop('checked', false);
            }
        } else if ('icon' === column.type) {
            if ('create' === mode) {
                $wrapper.append($('<span/>')
                    .addClass(data.uiLibrary === 'bootstrap' ? 'glyphicon' : 'ui-icon')
                    .addClass(column.icon).css({ 'cursor': 'pointer' }));
            }
        } else if (column.tmpl) {
            text = column.tmpl;
            column.tmpl.replace(/\{(.+?)\}/g, function ($0, $1) {
                text = text.replace($0, gj.grid.private.formatText(record[$1], column));
            });
            $wrapper.text(text);
        } else {
            gj.grid.private.setCellText($wrapper, column, record[column.field]);
        }
        if (column.tooltip && 'create' === mode) {
            $wrapper.attr('title', column.tooltip);
        }

        if ('update' === mode) {
            $cell.off();
            $wrapper.off();
        }
        if (column.events) {
            for (var key in column.events) {
                if (column.events.hasOwnProperty(key)) {
                    $cell.on(key, { id: id, field: column.field, record: record }, column.events[key]);
                }
            }
        }
        if (column.hidden) {
            $cell.hide();
        }

        gj.grid.events.cellDataBound($grid, $wrapper, id, column, record);

        return $cell;
    },

    setCellText: function ($wrapper, column, value) {
        var text = gj.grid.private.formatText(value, column);
        if (!column.tooltip) {
            $wrapper.attr('title', text);
        }
        $wrapper.text(text);
    },

    formatText: function (text, column) {
        var dt, day, month;
        if (text && column.type) {
            switch (column.type) {
                case "date":
                    if (text.indexOf("/Date(") > -1) {
                        dt = new Date(parseInt(text.substr(6), 10));
                    } else {
                        var parts = text.match(/(\d+)/g);
                        // new Date(year, month, date, hours, minutes, seconds);
                        dt = new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]); // months are 0-based
                    }

                    if (dt.format && column.format) {
                        text = dt.format(column.format); //using 3rd party plugin "Date Format 1.2.3 by (c) 2007-2009 Steven Levithan <stevenlevithan.com>"
                    } else {
                        day = dt.getDate().toString().length === 2 ? dt.getDate() : "0" + dt.getDate();
                        month = (dt.getMonth() + 1).toString();
                        month = month.length === 2 ? month : "0" + month;
                        text = month + "/" + day + "/" + dt.getFullYear();
                    }
                    break;
            }
        } else {
            text = (typeof (text) === "undefined" || text === null) ? "" : text.toString();
        }
        if (column.decimalDigits && text) {
            text = parseFloat(text).toFixed(column.decimalDigits);
        }
        return text;
    },

    GetRecords: function (data, response) {
        var records = [];
        if ($.isArray(response)) {
            records = response;
        } else if (data && data.mapping && $.isArray(response[data.mapping.dataField])) {
            records = response[data.mapping.dataField];
        }
        return records;
    },

    CreateRowClickHandler: function ($grid, id, record) {
        return function (e) {
            gj.grid.private.setSelected($grid, $(this), id);
        };
    },

    SelectRow: function ($grid, data, $row) {
        $row.addClass(data.style.content.rowSelected);

        gj.grid.events.rowSelect($grid, $row, $row.data("row").id, $row.data("row").record);

        if ("checkbox" === data.selectionMethod) {
            $row.find("td:nth-child(1) input[type='checkbox']").prop("checked", true);
        }
    },

    UnselectRow: function ($grid, data, $row) {
        if ($row.hasClass(data.style.content.rowSelected)) {
            $row.removeClass(data.style.content.rowSelected);

            gj.grid.events.rowUnselect($grid, $row, $row.data("row").id, $row.data("row").record)

            if ("checkbox" === data.selectionMethod) {
                $row.find("td:nth-child(1) input[type='checkbox']").prop("checked", false);
            }
        }
    },

    setSelected: function ($grid, $row, id) {
        var data = $grid.data('grid');
        if ($row.hasClass(data.style.content.rowSelected)) {
            gj.grid.private.UnselectRow($grid, data, $row);
        } else {
            if ("single" === data.selectionType) {
                $row.siblings().each(function () {
                    gj.grid.private.UnselectRow($grid, data, $(this));
                });
            }
            gj.grid.private.SelectRow($grid, data, $row);
        }
    },

    _GetSelected: function ($grid) {
        var result, data, selections;
        data = $grid.data("grid");
        selections = $grid.find("tbody > tr." + data.style.content.rowSelected);
        if (selections.length > 0) {
            result = $(selections[0]).data("row").id;
        }
        return result;
    },

    GetSelectedRows: function ($grid) {
        var data = $grid.data("grid");
        return $grid.find("tbody > tr." + data.style.content.rowSelected);
    },

    _GetSelections: function ($grid) {
        var result = [],
            $selections = gj.grid.private.GetSelectedRows($grid);
        if (0 < $selections.length) {
            $selections.each(function () {
                result.push($(this).data("row").id);
            });
        }
        return result;
    },

    getRecordById: function ($grid, id) {
        var result = {}, rows, i, rowData;
        rows = $grid.find("tbody > tr");
        for (i = 0; i < rows.length; i++) {
            rowData = $(rows[i]).data("row");
            if (rowData.id === id) {
                result = rowData.record;
                break;
            }
        }
        return result;
    },

    getRowById: function ($grid, id) {
        var result = null, rows, i, rowData;
        rows = $grid.find("tbody > tr");
        for (i = 0; i < rows.length; i++) {
            rowData = $(rows[i]).data("row");
            if (rowData.id === id) {
                result = $(rows[i]);
                break;
            }
        }
        return result;
    },

    getByPosition: function ($grid, position) {
        var result = {}, $rows, data;
        $rows = $grid.find("tbody > tr");
        if ($rows.length >= position) {
            data = $rows.eq(position - 1).data("row");
            if (data && data.record) {
                result = data.record;
            }
        }
        return result;
    },

    GetColumnPosition: function (columns, field) {
        var position = -1, i;
        for (i = 0; i < columns.length; i++) {
            if (columns[i].field === field) {
                position = i;
                break;
            }
        }
        return position;
    },

    GetColumnInfo: function ($grid, field) {
        var i, result = {}, data = $grid.data("grid");
        for (i = 0; i < data.columns.length; i += 1) {
            if (data.columns[i].field === field) {
                result = data.columns[i];
                break;
            }
        }
        return result;
    },

    GetCell: function ($grid, id, index) {
        var result = {}, rows, i, rowData, position;
        position = gj.grid.private.GetColumnPosition($grid, index);
        rows = $grid.find("tbody > tr");
        for (i = 0; i < rows.length; i += 1) {
            rowData = $(rows[i]).data("row");
            if (rowData.id === id) {
                result = $(rows[i].cells[position]).find("div");
                break;
            }
        }
        return result;
    },

    SetCellContent: function ($grid, id, index, value) {
        var column, $cellWrapper = gj.grid.private.GetCell($grid, id, index);
        $cellWrapper.empty();
        if (typeof (value) === "object") {
            $cellWrapper.append(value);
        } else {
            column = gj.grid.private.GetColumnInfo($grid, index);
            gj.grid.private.setCellText($cellWrapper, column, value);
        }
    },

    clone: function (source) {
        var target = [];
        $.each(source, function () {
            target.push(this.clone());
        });
        return target;
    },

    GetAll: function ($grid) {
        var result = [],
                rows = $grid.find("tbody > tr"),
                i, record;

        for (i = 0; i < rows.length; i++) {
            record = $(rows[i]).data("row");
            if (record) {
                result.push(record);
            }
        }
        return result;
    },

    countVisibleColumns: function ($grid) {
        var columns, count, i;
        columns = $grid.data('grid').columns;
        count = 0;
        for (i = 0; i < columns.length; i++) {
            if (columns[i].hidden !== true) {
                count++;
            }
        }
        return count;
    }
};


gj.grid.public = {
    xhr: null,


    reload: function (params) {
        var data, ajaxOptions, records;
        data = this.data('grid');
        $.extend(data.params, params);
        gj.grid.private.StartLoading(this);
        if ($.isArray(data.dataSource)) {
            records = gj.grid.private.GetRecords(data, data.dataSource);
            gj.grid.private.loadData(this, records, records.length);
        } else if (typeof (data.dataSource) === "string") {
            ajaxOptions = { url: data.dataSource, data: data.params, success: gj.grid.private.LoaderSuccessHandler(this) };
            if (this.xhr) {
                this.xhr.abort();
            }
            this.xhr = $.ajax(ajaxOptions);
        } else if (typeof(data.dataSource) === "object") {
            if (!data.dataSource.data) {
                data.dataSource.data = {};
            }
            $.extend(data.dataSource.data, data.params);
            ajaxOptions = $.extend(true, {}, data.dataSource); //clone dataSource object
            if (ajaxOptions.dataType === "json" && typeof (ajaxOptions.data) === "object") {
                ajaxOptions.data = JSON.stringify(ajaxOptions.data);
            }
            if (!ajaxOptions.success) {
                ajaxOptions.success = gj.grid.private.LoaderSuccessHandler(this);
            }
            if (this.xhr) {
                this.xhr.abort();
            }
            this.xhr = $.ajax(ajaxOptions);
        }
        return this;
    },


    clear: function () {
        var data = this.data('grid');
        this.xhr && this.xhr.abort();
        if ("checkbox" === data.selectionMethod) {
            this.find("input#checkAllBoxes").hide();
        }
        this.children("tbody").empty();
        gj.grid.private.StopLoading(this);
        gj.grid.private.AppendEmptyRow(this, "&nbsp;");
        gj.grid.events.dataBound(this, [], 0);
        return this;
    },


    count: function () {
        return $(this).find('tbody tr[data-role="row"]').length;
    },


    render: function (response) {
        var data, records, totalRecords;
        if (response) {
            data = this.data('grid');
            if (data) {
                records = gj.grid.private.GetRecords(data, response);
                totalRecords = response[data.mapping.totalRecordsField];
                if (!totalRecords || isNaN(totalRecords)) {
                    totalRecords = 0;
                }
                gj.grid.private.loadData(this, records, totalRecords);
            }
        }
    },


    destroy: function (keepTableTag, keepWrapperTag) {
        var data = this.data('grid');
        if (data) {
            gj.grid.events.destroying(this);
            gj.grid.private.StopLoading(this);
            this.xhr && this.xhr.abort();
            this.off();
            if (keepWrapperTag === false && this.parent('div[data-role="wrapper"]').length > 0) {
                this.unwrap();
            }
            this.removeData();
            if (keepTableTag === false) {
                this.remove();
            } else {
                this.removeClass().empty();
            }
        }
    },

    setSelected: function (id) {
        var $row = gj.grid.private.getRowById(this, id);
        if ($row) {
            gj.grid.private.setSelected(this, $row, id);
        }
    },


    getSelected: function () {
        return gj.grid.private._GetSelected(this);
    },


    getSelections: function () {
        return gj.grid.private._GetSelections(this);
    },


    selectAll: function () {
        var $grid = this,
            data = this.data('grid');
        $grid.find("thead input#checkAllBoxes").prop("checked", true);
        $grid.find("tbody tr").each(function () {
            gj.grid.private.SelectRow($grid, data, $(this));
        });
    },


    unSelectAll: function () {
        var $grid = $(this),
            data = this.data('grid');
        this.find("thead input#checkAllBoxes").prop("checked", false);
        this.find("tbody tr").each(function () {
            gj.grid.private.UnselectRow($grid, data, $(this));
        });
    },


    getById: function (id) {
        return gj.grid.private.getRecordById(this, id);
    },


    get: function (position) {
        return gj.grid.private.getByPosition(this, position);
    },


    getAll: function () {
        return gj.grid.private.GetAll(this);
    },


    showColumn: function (field) {
        var data = this.data('grid'),
            position = gj.grid.private.GetColumnPosition(data.columns, field),
            $cells;

        if (position > -1) {
            this.find("thead>tr>th:eq(" + position + ")").show();
            $.each(this.find("tbody>tr"), function () {
                $(this).find("td:eq(" + position + ")").show();
            });
            data.columns[position].hidden = false;

            $cells = this.find('tbody > tr[data-role="empty"] > td');
            if ($cells && $cells.length) {
                $cells.attr('colspan', gj.grid.private.countVisibleColumns(this));
            }

            gj.grid.events.columnShow(this, data.columns[position]);
        }

        return this;
    },

    hideColumn: function (field) {
        var data = this.data('grid'),
            position = gj.grid.private.GetColumnPosition(data.columns, field),
            $cells;

        if (position > -1) {
            this.find("thead>tr>th:eq(" + position + ")").hide();
            $.each(this.find("tbody>tr"), function () {
                $(this).find("td:eq(" + position + ")").hide();
            });
            data.columns[position].hidden = true;

            $cells = this.find('tbody > tr[data-role="empty"] > td');
            if ($cells && $cells.length) {
                $cells.attr('colspan', gj.grid.private.countVisibleColumns(this));
            }

            gj.grid.events.columnHide(this, data.columns[position]);
        }

        return this;
    },


    addRow: function (record) {
        var position, $rows = this.find('tbody > tr');
        //clear empty row if exists
        if ($rows.length === 1 && $rows.data('role') === 'empty') {
            $rows.remove();
        }
        position = this.count();
        gj.grid.private.RowRenderer(this, null, record, position);
        return this;
    },


    updateRow: function (id, record) {
        var $row = gj.grid.private.getRowById(this, id);
        gj.grid.private.RowRenderer(this, $row, record, $row.index());
        return this;
    },


    setCellContent: function (id, index, value) {
        gj.grid.private.SetCellContent(this, id, index, value);
    },


    removeRow: function (id) {
        var $row = gj.grid.private.getRowById(this, id);
        if ($row) {
            gj.grid.events.rowRemoving(this, $row, id, $row.data('row'));
            $row.remove();
            if (this.count() == 0) {
                gj.grid.private.AppendEmptyRow(this);
            }
        }
        return this;
    }
};

(function ($) {
    $.fn.grid = function (method) {
        if (typeof method === 'object' || !method) {
            function Grid() {
                var self = this;
                $.extend(self, gj.grid.public);
            };
            var grid = new Grid();
            $.extend(this, grid);
            return gj.grid.private.init.apply(this, arguments);
        } else if (gj.grid.public[method]) {
            return gj.grid.public[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else {
            throw 'Method ' + method + ' does not exist.';
        }
    };
})(jQuery);

